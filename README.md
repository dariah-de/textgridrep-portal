# TextGrid Repository Portal

The portal is a web frontend to access and browse data published in the [TextGrid-Repository](https://textgridrep.org). [TextGrid](https://textgrid.de) is a virtual research environment for the humanities. This software uses the public [TextGrid APIs](https://textgridlab.org/doc/services/index.html).

This is the third iteration of the software. Previous versions:
* JavaScript/HTML: https://gitlab.gwdg.de/dariah-de/textgridrep/textgridrep-webseite
* Liferay based: https://gitlab.gwdg.de/dariah-de/textgridrep/liferay-textgrid

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need Java (at least in version 8) and Gradle to run a development version.

        sudo apt install openjdk-8-jdk

This project is build with gradle. The gradle wrapper script `gradlew` will take care of installing gradle if not available.

### Installing

Run a local version of the portal:

        ./gradlew bootRun

Access the portal at http://localhost:8080/ afterwards.

### Configuration

Edit `src/main/resources/application.properties` to change configuration. You may also override default settings from [application.properties](./src/main/resources/application.properties) with environment variables. To change, for example, the textgrid server endpoint and activate sandbox mode:

        TEXTGRID_HOST=https://dev.textgridlab.org SANDBOX_ENABLED=true ./gradlew bootRun

or to change the loglevel:

        LOGGING_LEVEL_INFO_TEXTGRID_REP=DEBUG ./gradlew bootRun

## Running the tests

Running the tests requires internet access and the services at https://textgridlab.org to be available.

        ./gradlew test

Test output is written to `build/reports/tests/test/index.html`

## Development Server / Continuous Build

The gradle build file has the spring-boot-devtools included, which [allow live reloading when classpath changes](https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto-hotswapping). This works fine when web resources like jsp files or the classpath change. It does not include automatic recompilation when Java code changes. This is what you IDE may do, or gradle with its [continuous build feature](https://blog.gradle.org/introducing-continuous-build).

For the latter I recommend to open two terminal windows to run two commands in parallel. One for the continuous build:

        ./gradlew classes -t

and one for running the spring-boot dev server:

        ./gradlew bootRun

# Release process

We use [git-flow](https://danielkummer.github.io/git-flow-cheatsheet/index.de_DE.html).

        git checkout develop
        git flow release start 3.1.11

edit `build.gradle` and set version number, afterwards:

        git commit -am "set version 3.1.11"
        git flow release finish 3.1.11

edit `build.gradle` and set version number to 3.1.12-SNAPSHOT, afterwards:

        git commit -am "start 3.1.12-SNAPSHOT"
        git push origin develop main --tags

This pushes a new tag 3.1.11. Creating the tag will trigger the relase-build, which will be deployed to https://textgridrep.org

in gitlab-ci go to https://gitlab.gwdg.de/dariah-de/textgridrep-portal/-/releases, click on "New Release"

* Tag name: 3.1.11
* Release title: 3.1.11
* Milestones: 3.1.11
* Release notes:

        * documents added how to use annotatiod
        * fixed timeout and notification about slow XM transformations

Afterwards close Milestone 3.1.11 and create a new Milestone 3.1.12 in https://gitlab.gwdg.de/dariah-de/textgridrep-portal/-/milestones


## bugfix release

        git flow hotfix start 3.1.10.1

apply fixes, or cherry pick, and set new version number in `build.gradle`

        git commit -am "set version 3.1.10.1"
        git flow hotfix finish 3.1.10.1

note: this may result in merge conflict in develop branch, as `build.gradle`
has different version numbers now.

        git push origin develop main --tags

in gitlab-ci go to https://gitlab.gwdg.de/dariah-de/textgridrep-portal/-/releases, click on "New Release"

* Tag name: 3.1.10.1
* Release title: 3.1.10.1
* Release notes:

        * fix links in index.en.md

