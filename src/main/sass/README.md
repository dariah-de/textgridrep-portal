# Theme for textgridrep.org

Originating from the [Liferay 6 Theme for textgrid.de](https://projects.gwdg.de/projects/liferay-textgrid)

Note: the Sass was formerly built with gulp and made use of globbing. This
is not supported by sass-dart, so we now have an _index.sass in every subdir.

## Extending the theme (aka adding styles)

Styles are written in Sass (indented syntax), using a variant of the
[BEM](https://en.bem.info/method/) naming scheme for class names. In BEM -
meaning block, element, modifier - all styles belong to one of these types:
- `.block` represents the higher level of an abstraction or component
- `.block_element`, divided by `_`, represents a descendent of .block. Use only
  one level of descendency, it does not have to correspond with the actual DOM.
- `.-modifier`, prepended by `-` (think command-line switch), represents a
  different state of a .block or .block_element and can only be used
  alongside those.

In Sass, this looks like `.namespace.block-name_element-name &.-modifier-name.`
Namespace is added to improve specifity and thus override default styles. This
corresponds to
`<element class="namespace block-name_element-name -modifier-name"`> in
HTML/Velocity. Up to two class-less child elements are fine if otherwise extra
overrides would be necessary. Use !important only for base theme overrides
as a last resort.
