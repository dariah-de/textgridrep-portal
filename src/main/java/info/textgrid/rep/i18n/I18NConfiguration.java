package info.textgrid.rep.i18n;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * This configuration handles language switching with spring.
 * Language will be first determined by the http "Accept-Language"
 * request header from the browser. Using information from the
 * http request header works because of not Setting the
 * DefaultLocale on the SessionLocaleResolver.
 *
 * The parameter ?lang is used to change language manually,
 * which causes a cookie to be set which spring uses on
 * further requests.
 *
 * @author Ubbo Veentjer
 */
@Configuration
public class I18NConfiguration implements WebMvcConfigurer {

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
      registry.addInterceptor(localeChangeInterceptor());
  }

  @Bean
  public LocaleChangeInterceptor localeChangeInterceptor() {
      LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
      lci.setParamName("lang");
      return lci;
  }

  @Bean
  public LocaleResolver localeResolver() {
      SessionLocaleResolver slr = new SessionLocaleResolver();
      return slr;
  }

}
