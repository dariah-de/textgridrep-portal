package info.textgrid.rep.i18n;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * Utility class to keep track of available and requested language.
 *
 * Available and default language are configured by lang.available
 * and lang.default spring configuration properties. The translations
 * are loaded from i18n/Language_xx.properties
 *
 * This class keeps the compatibility to jsp code accessing an
 * i18n hashmap, which originated in porting
 * JavaServerFaces el-i18n functionality.
 *
 * @author Ubbo Veentjer
 */
@Component
public class I18NProvider {

  @Value("${lang.available}")
  private List<String> langAvailable;

  @Value("${lang.default}")
  private String langDefault;

  private HashMap<String, I18N> i18nMap = new HashMap<String, I18N>();

  @PostConstruct
  public void postConstruct() {
    for (String lang : langAvailable) {
      I18N i18n = new I18N(lang, I18NUtils.getTranslationMap(new Locale(lang)));
      i18nMap.put(lang, i18n);
    }
  }

  /**
   * Get matching i18n object for a locale
   *
   * @param locale
   * @return i18n object for a locale
   */
  public I18N getI18N(Locale locale) {
    Locale selectedLocale;
    if(langAvailable.contains(locale.getLanguage())) {
      selectedLocale = locale;
    } else {
      selectedLocale = new Locale(langDefault);
    }
    return i18nMap.get(selectedLocale.getLanguage());
  }

  public List<String> getLangAvailable() {
    return langAvailable;
  }

  public void setLangAvailable(List<String> langAvailable) {
    this.langAvailable = langAvailable;
  }

  public String getLangDefault() {
    return langDefault;
  }

  public void setLangDefault(String langDefault) {
    this.langDefault = langDefault;
  }

}
