package info.textgrid.rep.i18n;

import java.util.HashMap;


/**
 * Keep track of language map and its name string for use with jsp controllers.
 *
 * This class keeps the compatibility to jsp code accessing an i18n hashmap,
 * which originated in porting Java ServerFaces EL #i18n functionality.
 *
 * @author Ubbo Veentjer
 */
public class I18N {

  private String language;
  private HashMap<String, String> messages;

  public String getLanguage() {
    return language;
  }

  public I18N(String language, HashMap<String, String> messages) {
    this.language = language;
    this.messages = messages;
  }

  /**
   * convenience function for accessing single entry of the messages hashmap
   * @param key
   * @return
   */
  public String get(String key) {
    return this.messages.get(key);
  }

  public HashMap<String, String> getTranslationMap() {
    return messages;
  }
}
