package info.textgrid.rep.advancedsearch;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdvancedSearchController {

  private static final Log log = LogFactory.getLog(AdvancedSearchController.class);

  @GetMapping("/advanced-search")
  public String render(Model model) {
    return "advancedsearch";
  }

}
