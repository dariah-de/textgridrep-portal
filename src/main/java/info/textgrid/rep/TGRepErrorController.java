package info.textgrid.rep;

import java.util.Locale;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TGRepErrorController implements ErrorController {

  @RequestMapping("/error")
  public String handleError(Model model, Locale locale, HttpServletRequest request) {

    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

    if (status != null) {
      Integer statusCode = Integer.valueOf(status.toString());
      if(statusCode == HttpStatus.NOT_FOUND.value()) {
        return "error404";
      }
    }

    if(request.getAttribute("javax.servlet.error.exception") != null) {
      Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
      model.addAttribute("exception", exception.getMessage());
      model.addAttribute("stacktrace", exception.getStackTrace());
    }
    model.addAttribute("statusCode", request.getAttribute("javax.servlet.error.status_code"));

    return "error";
  }

}
