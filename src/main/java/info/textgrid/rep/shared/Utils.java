package info.textgrid.rep.shared;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3._1999._02._22_rdf_syntax_ns_.RdfType;
import org.w3c.dom.Element;
import info.textgrid.namespaces.metadata.core._2010.RelationType;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

public class Utils {

  private final static int THUMBSIZE = 250;
  private final static String BROWSE_URL = "/browse/";
  private final static String SEARCH_URL = "/search";

  private final static String MIME_IMG_BASE = "/images/tg-icons/";
  protected final static String MIME_IMG_XML = MIME_IMG_BASE + "x14-document-xml.svg";
  protected final static String MIME_IMG_WORK = MIME_IMG_BASE + "130-ist_Werk.svg";
  protected final static String MIME_IMG_EDITION = MIME_IMG_BASE + "108-ist-Edition.svg";
  protected final static String MIME_IMG_COLLECTION = MIME_IMG_BASE + "109-ist-Collection.svg";
  protected final static String MIME_IMG_AGGREGATION = MIME_IMG_BASE + "072-zeige-AggregationPerspektive.svg";
  protected final static String MIME_IMG_TEXT = MIME_IMG_BASE + "x11-document-text.svg";



  private static final Log log = LogFactory.getLog(Utils.class);

  public static String browseUrl(String tgurl) {
    return BROWSE_URL + urlmod(tgurl);
  }

  public static String searchUrl(String mode, String query, List<String> filter, String order,
      int start, int limit) {
    String querystring = query.replace("\"", "&quot;") + getFilterQueryString(filter);
    return SEARCH_URL + "?query=" + querystring + "&start=" + start + "&limit=" + limit + "&order="
        + order + "&mode=" + mode;
  }

  public static String urlmod(String tgurl) {
    if (tgurl.length() > 10) {
      return tgurl.substring(9);
    }
    return "tgurl";
  }

  public static String getImageUrl(String textgridHost, ResultType tgsearchresult) {
    return getImageUrl(textgridHost, tgsearchresult, false);
  }

  public static String getGalleryImageUrl(String textgridHost, ResultType tgsearchresult) {
    return getImageUrl(textgridHost, tgsearchresult, true);
  }

  private static String getImageUrl(String textgridHost, ResultType tgsearchresult, boolean gallery) {

    String imageUri = null;
    String format = "";
    try {
      format = tgsearchresult.getObject().getGeneric().getProvided().getFormat();
    } catch (NullPointerException e) {
      log.error("NPE getting format from " +tgsearchresult.getTextgridUri() + " - Check the SearchIndex!");
    }

    if (format.contains("image")) {
      imageUri = tgsearchresult.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    } else if (format.contains("tg.collection")) {
      // collections may have author images linked in the "digitale bibliothek"
      imageUri = getImageFromCollectionRdf(tgsearchresult);
    }

    if (imageUri != null) {
      return textgridHost + "/1.0/digilib/rest/IIIF/" + imageUri + "/full/," + THUMBSIZE
          + "/0/native.jpg";
    } else if(gallery){
      return getImageForMimetype(format);
    } else {
      return "/images/no_image.svg";
    }
  }

  protected static String getImageForMimetype(String format) {
    String img = "";
    switch(format) {
      case "text/xml":
        img = MIME_IMG_XML;
        break;
      case "text/tg.work+xml":
        img = MIME_IMG_WORK;
        break;
      case "text/tg.aggregation+xml":
        img = MIME_IMG_AGGREGATION;
        break;
      case "text/tg.collection+tg.aggregation+xml":
        img = MIME_IMG_COLLECTION;
        break;
      case "text/tg.edition+tg.aggregation+xml":
        img = MIME_IMG_EDITION;
        break;
      default:
        if(format.startsWith("text/")) {
          img = MIME_IMG_TEXT;
        } else {
          img = "";
        };
    }
    return img;
  }

  private static String getImageFromCollectionRdf(ResultType tgsearchresult) {
    String imageUri = null;
    RelationType rel = tgsearchresult.getObject().getRelations();
    if (rel != null) {
      RdfType rdf = rel.getRDF();
      if (rdf != null) {
        List<Object> rdfs = rdf.getAny();
        for (Object o : rdfs) {
          Element dom = (Element) o;
          imageUri = dom
              .getElementsByTagNameNS("http://textgrid.info/relation-ns#", "depiction")
              .item(0)
              .getAttributes()
              .getNamedItemNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource")
              .getTextContent();

        }
      }
    }
    return imageUri;
  }

  public static String getFilterQueryString(List<String> filters) {

    if (filters == null) {
      return "";
    }

    StringBuffer sb = new StringBuffer();
    for (String filter : filters) {
      try {
        filter = URLEncoder.encode(filter, "UTF-8");
      } catch (UnsupportedEncodingException e) {
        log.error("could not urlencode the string: " + filter);
      }
      sb.append("&filter=").append(filter).append("");
    }
    return sb.toString();
  }

  public static String urlencode(String string) {
    try {
      string = URLEncoder.encode(string, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      log.error("could not urlencode the string: " + string);
    }
    return string;
  }

  public static String replaceAll(String string, String pattern, String replacement) {
    return string.replaceAll(pattern, replacement);
  }

}
