package info.textgrid.rep.shared;

import java.io.Serializable;

public class ToolLink implements Serializable {

  private static final long serialVersionUID = -5848034800808088323L;

  private String url;
  private String label;
  private boolean active;
  private String cssClass;
  private String helpLink;

  public ToolLink(String label, String url, boolean active) {
    this.label = label;
    this.url = url;
    this.active = active;
  }

  public ToolLink(String label, String url, boolean active, String cssClass) {
    this.label = label;
    this.url = url;
    this.active = active;
    this.setCssClass(cssClass);
  }

  public String getUrl() {
    return url;
  }

  public ToolLink setUrl(String url) {
    this.url = url;
    return this;
  }

  public String getLabel() {
    return label;
  }

  public ToolLink setLabel(String label) {
    this.label = label;
    return this;
  }

  public boolean isActive() {
    return active;
  }

  public ToolLink setActive(boolean active) {
    this.active = active;
    return this;
  }

  public String getCssClass() {
    return cssClass;
  }

  public ToolLink setCssClass(String cssClass) {
    this.cssClass = cssClass;
    return this;
  }

  public String getHelpLink() {
    return helpLink;
  }

  public ToolLink setHelpLink(String helpLink) {
    this.helpLink = helpLink;
    return this;
  }

}
