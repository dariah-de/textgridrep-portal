package info.textgrid.rep.shelf;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.rep.i18n.I18N;
import info.textgrid.rep.i18n.I18NProvider;
import info.textgrid.rep.service.TgsearchClientService;
import info.textgrid.rep.shared.ToolLink;

@Controller
@SessionAttributes("shelf")
public class ShelfController {

  private TgsearchClientService tgsearchClientService;
  private I18NProvider i18nProvider;

  private static final Log log = LogFactory.getLog(ShelfController.class);

  @Autowired
  public ShelfController(TgsearchClientService tgsearchClientService, I18NProvider i18nProvider) {
    this.tgsearchClientService = tgsearchClientService;
    this.i18nProvider = i18nProvider;
  }

  @ModelAttribute("shelf")
  public Shelf getShelf() {
      return new Shelf();
  }

  @GetMapping("/shelf")
  public String render(
      Model model,
      Locale locale,
      @ModelAttribute Shelf shelf,
      @RequestParam(value="mode", required=false) String mode) {

    I18N i18n = i18nProvider.getI18N(locale);

    model.addAttribute("mode", mode);

    List<ResultType> results = new ArrayList<ResultType>();

    if(shelf != null) {
        for(String textgridUri : shelf.getItems()) {
            ResultType res = tgsearchClientService.getMetadata(textgridUri);
            results.add(res);
        }
    }

    List<ToolLink> viewmodes = new ArrayList<ToolLink>();
    viewmodes.add(new ToolLink(i18n.get("list"), "/shelf?mode=list", false));
    viewmodes.add(new ToolLink(i18n.get("gallery"), "/shelf?mode=gallery", false));
    model.addAttribute("viewmodes", viewmodes);

    model.addAttribute("results", results);
    model.addAttribute("shelfItemString", shelf.getItemsAsString());
    model.addAttribute("projectmap", tgsearchClientService.getProjectMap(true));

    return "shelf";

  }

}
