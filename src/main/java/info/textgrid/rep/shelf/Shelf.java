package info.textgrid.rep.shelf;

import java.util.ArrayList;
import java.util.List;

public class Shelf {

  private List<String> items = new ArrayList<String>();

  public List<String> getItems() {
    return items;
  }

  public int addItem(String item) {
    items.add(item);
    return items.size();
  }

  public long remove(String textgridUri) {
    items.remove(textgridUri);
    return items.size();
  }

  public String getItemsAsString() {
    return String.join(",", items);
  }

  public long clear() {
    items.clear();
    return items.size();
  }

  public long size() {
    return items.size();
  }

  public boolean contains(String textgridUri) {
    return items.contains(textgridUri);
  }


}
