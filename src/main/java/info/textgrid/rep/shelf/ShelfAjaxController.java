package info.textgrid.rep.shelf;

import java.io.IOException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;


@RestController
@SessionAttributes("shelf")
@RequestMapping("/service/shelf")
public class ShelfAjaxController {

  @ModelAttribute("shelf")
  public Shelf getShelf() {
      return new Shelf();
  }

  @PostMapping("/add")
  public int addToShelf(
      @ModelAttribute Shelf shelf,
      @RequestParam("uri") String textgridUri
      ) throws IOException {

    return shelf.addItem(textgridUri);

  }

  @PostMapping("/remove")
  public long removeFromBasket(
      @ModelAttribute Shelf shelf,
      @RequestParam("uri") String textgridUri
      ) throws IOException {

    return shelf.remove(textgridUri);

  }

  @PostMapping("/clear")
  public long clearShelf(@ModelAttribute Shelf shelf) {
    return shelf.clear();
  }

  @GetMapping("/count")
  public long shelfCount(@ModelAttribute Shelf shelf) {
    return shelf.size();
  }

}
