package info.textgrid.rep.service;

import jakarta.annotation.PostConstruct;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TgrepConfigurationService {

  private static final Log log = LogFactory.getLog(TgrepConfigurationService.class);

  @Value("${textgrid.host}")
  private String textgridHost;
  @Value("${tgsearch.url}")
  private String tgsearchUrl;
  @Value("${handle.host}")
  private String handleHost;
  @Value("${tool.voyant.host}")
  private String voyantHost;
  @Value("${tool.annotate.host}")
  private String toolAnnotateHost;
  @Value("${tool.mirador.host}")
  private String toolMiradorHost;
  @Value("${tool.digilib.host}")
  private String toolDigilibHost;
  @Value("${tool.switchboard.host}")
  private String toolSwitchboardHost;
  @Value("${sandbox.enabled}")
  private Boolean sandboxEnabled;
  @Value("${sentry.enabled}")
  private Boolean sentryEnabled;
  @Value("${tracking.enabled}")
  private Boolean trackingEnabled;
  @Value("${aggregator.timeout.connect}")
  private long aggregatorConnectTimeout;
  @Value("${aggregator.timeout.read}")
  private long aggregatorReadTimeout;
  @Value("${css.build.timestamp}")
  private String cssBuildTimestamp;

  public String getTextgridHost() {
    return textgridHost;
  }

  public String getTgsearchUrl() {
    return tgsearchUrl;
  }

  public String getHandleHost() {
    return handleHost;
  }

  public String getVoyantHost() {
    return voyantHost;
  }

  public String getToolAnnotateHost() {
    return toolAnnotateHost;
  }

  public String getToolMiradorHost() {
    return toolMiradorHost;
  }

  public String getToolDigilibHost() {
    return toolDigilibHost;
  }

  public String getToolSwitchboardHost() {
    return toolSwitchboardHost;
  }

  public Boolean getSandboxEnabled() {
    return sandboxEnabled;
  }

  public Boolean getSentryEnabled() {
    return sentryEnabled;
  }

  public Boolean getTrackingEnabled() {
    return trackingEnabled;
  }

  public long getAggregatorConnectTimeout() {
    return aggregatorConnectTimeout;
  }

  public long getAggregatorReadTimeout() {
    return aggregatorReadTimeout;
  }

  // composed
  public String getAggregatorUrl() {
    return getTextgridHost()+"/1.0/aggregator";
  }

  public String getCssBuildTimestamp() {
    return cssBuildTimestamp;
  }

  public void setCssBuildTimestamp(String cssBuildTimestamp) {
    this.cssBuildTimestamp = cssBuildTimestamp;
  }

  @PostConstruct
  public void postConstruct() {
    log.info("textgrid host:    " + textgridHost);
    log.info("tgsearch url:     " + tgsearchUrl);
    log.info("sandbox enabled:  " + sandboxEnabled);
    log.info("handle host:      " + handleHost);
    log.info("voyant host:      " + voyantHost);
    log.info("annotate host:    " + toolAnnotateHost);
    log.info("mirador host:     " + toolMiradorHost);
    log.info("digilib host:     " + toolDigilibHost);
    log.info("switchboard host: " + toolSwitchboardHost);
  }

}
