package info.textgrid.rep.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="No metadata found for this URI")
public class NothingFoundException extends RuntimeException {

  private static final long serialVersionUID = 6908289670791711296L;

}


