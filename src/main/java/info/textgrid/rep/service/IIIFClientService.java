package info.textgrid.rep.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.jakarta.rs.json.JacksonJsonProvider;

import info.textgrid.rep.browse.IIIFProjects;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

@Service
public class IIIFClientService {

  private TgrepConfigurationService tgrepConfig;
  private WebTarget manifestTarget;

  @Autowired
  public IIIFClientService(TgrepConfigurationService tgrepConfig) {
    this.tgrepConfig = tgrepConfig;
    this.setupClient();
  }

  private void setupClient() {
    this.manifestTarget = ClientBuilder
        .newClient()
        .register(JacksonJsonProvider.class)
        .target(tgrepConfig.getTextgridHost())
        .path("/1.0/iiif/manifests/projects/");
  }

  /**
   * get a list of projects from iiif manifest service
   * which have iiif manifest generation enabled
   * @return list of iiif enabled projects
   */
  @Cacheable("iiif_enabled_projects")
  public List<String> getManifests() {
    var p = this.manifestTarget
        .request()
        .get(IIIFProjects.class);
    return p.projects;
  }

}
