package info.textgrid.rep.service;

import java.io.InputStream;
import jakarta.annotation.PostConstruct;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import info.textgrid.clients.CrudClient;
import info.textgrid.clients.tgcrud.CrudClientException;

@Service
public class TgcrudClientService {

  private TgrepConfigurationService tgrepConfig;
  private CrudClient crudClient;

  private static final Log log = LogFactory.getLog(TgsearchClientService.class);

  @Autowired
  public TgcrudClientService(TgrepConfigurationService tgrepConfig) throws CrudClientException {
    this.tgrepConfig = tgrepConfig;
    this.setupClient();
  }

  private void setupClient() throws CrudClientException {
    log.info("setting up tgsearch client for host: " + tgrepConfig.getTextgridHost());
    // Create CrudClient with GZIP compression enabled
    crudClient = new CrudClient(tgrepConfig.getTextgridHost() + "/1.0/tgcrud-public/rest");
  }

  public InputStream read(String textgridUri) throws CrudClientException {
    return this.crudClient.read().setTextgridUri(textgridUri).execute().getData();
  }

  public CrudClient getClient() {
    return crudClient;
  }

}
