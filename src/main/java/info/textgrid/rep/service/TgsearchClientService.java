package info.textgrid.rep.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import info.textgrid.clients.SearchClient;
import info.textgrid.namespaces.metadata.portalconfig._2020_06_16.Html;
import info.textgrid.namespaces.middleware.tgsearch.FacetResponse;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.namespaces.middleware.tgsearch.Revisions;
import info.textgrid.namespaces.middleware.tgsearch.portal.Project;
import info.textgrid.namespaces.middleware.tgsearch.portal.ProjectsResponse;
import jakarta.ws.rs.WebApplicationException;

@Service
public class TgsearchClientService {

  @Value("${tgsearch.query.facetlimit}")
  private int facetLimit = 10;

  private TgrepConfigurationService tgrepConfig;
  private SearchClient searchClient;

  private static final Log log = LogFactory.getLog(TgsearchClientService.class);

  @Autowired
  public TgsearchClientService(TgrepConfigurationService tgrepConfig) {
    this.tgrepConfig = tgrepConfig;
    this.setupClient();
  }

  private void setupClient() {
    log.info("setting up tgsearch client for host: " + tgrepConfig.getTextgridHost());
    // Create Searchclient with GZIP compression enabled
    searchClient = new SearchClient(tgrepConfig.getTgsearchUrl());
  }

  public Response search(
      String query, String order, int start, int limit,
      List<String> facets, List<String> filter, boolean sandbox) {

    Response res = null;

    // when query is emtpy return everything
    if (query.equals("")) {
      query = "*";
    }

    try {
      res = searchClient.searchQuery()
          .setQuery(query)
          .setOrder(order)
          .setStart(start)
          .setLimit(limit)
          .setFacetList(facets)
          .setFacetLimit(facetLimit)
          .setFilterList(filter)
          .setSearchSandbox(sandbox)
          .setResolvePath(true)
          .execute();
    } catch (WebApplicationException e) {
      log.error(e.getClass().getSimpleName() + " thrown querying calling search(" + query + ", " + order + ", " + start
          + ", " + limit + ","
          + facets + ", " + filter + ", " + sandbox + "): " + e.getMessage());
    }

    return res;

  }

  public Response listRootCollections() {

    return searchClient.navigationQuery().listToplevelAggregations();

  }

  public Response listAggregation(String uri) {

    return searchClient.navigationQuery().listAggregation(uri);

  }

  public FacetResponse listFacet(List<String> facetList, int limit, String order, boolean sandbox) {

    return searchClient.facetQuery()
        .setTermList(facetList)
        .setLimit(limit)
        .setOrder(order)
        .setSearchSandbox(sandbox)
        .execute();

  }

  public ResultType getMetadata(String id) {

    List<ResultType> result = searchClient.infoQuery()
        .setResolvePath(true)
        .getMetadata(id)
        .getResult();

    if (result.size() == 0) {
      throw new NothingFoundException();
    }
    return result.get(0);

  }

  /**
   * check if there is a project specific xslt stylesheet configured for rendering
   * html,
   * also check if there are mime type (format) specific stylesheets configured
   *
   * @param metadata contains project ID and mimetype for looking up config
   * @return the xslt for mimetype or default xslt or empty string if nothing
   *         found
   */
  public String getProjectXsltUri(ResultType metadata) {
    String projectId = metadata.getObject().getGeneric().getGenerated().getProject().getId();
    String format = metadata.getObject().getGeneric().getProvided().getFormat();
    String defaultXsltUri = "";

    Project p = getProjectConfig(projectId, false); // TODO: use cache
    // JAXB.marshal(p, System.out);
    if (p.getPortalconfig() != null) {
      if (p.getPortalconfig().getXslt() != null) {
        for (Html html : p.getPortalconfig().getXslt().getHtml()) {
          if (html.getForMime() == null) {
            log.debug("default - " + html.getValue());
            defaultXsltUri = html.getValue();
          } else if (html.getForMime().equals(format)) {
            log.debug(html.getForMime() + " - " + html.getValue());
            // when we find specific xslt for mimetype we just return it
            return html.getValue();
          } else {
            log.debug(html.getForMime() + " - " + html.getValue());
          }
        }
        ;
      }
    }
    // if no xslt for mime is found we possibly found default or return empty string
    return defaultXsltUri;
  }

  // TODO: as getProjects is called normally, it would be better to cache
  // response!
  @Cacheable("search_project_config")
  public Project getProjectConfig(String projectId, boolean sandbox) {
    Project project = null;
    try {
      project = searchClient.projectQuery().setSearchSandbox(sandbox).projectDetails(projectId);
    } catch (WebApplicationException e) {
      log.error(e.getClass().getSimpleName() + " thrown calling getProjectConfig(" + projectId + ", " + sandbox
          + "): " + e.getMessage());
    }
    return project;
  }

  @Cacheable("search_project_map")
  public HashMap<String, Project> getProjectMap(boolean sandbox) {
    HashMap<String, Project> projects = new HashMap<String, Project>();
    ProjectsResponse pr = getProjects(sandbox);
    for (Project p : pr.getProjects()) {
      projects.put(p.getId(), p);
    }
    return projects;
  }

  @Cacheable("search_projects")
  public ProjectsResponse getProjects(boolean sandbox) {
    return searchClient.projectQuery().listProjects();
  }

  public Response listProjectsToplevelObjects(String id, int start, int limit, boolean sandbox) {
    Response res = null;
    try {
      res = searchClient.projectQuery().setSearchSandbox(sandbox).listToplevelAggregations(id, start, limit);
    } catch (WebApplicationException e) {
      log.error(e.getClass().getSimpleName() + " thrown calling listProjectsToplevelObjects(" + id + ", " + start
          + ", "+limit+"): " + e.getMessage());
    }
    return res;
  }

  public Revisions listRevisions(String id) {
    return searchClient.infoQuery().listRevisions(id);
  }

  public int latestRevision(String id) {
    return Collections.max(searchClient.infoQuery().listRevisions(id).getRevision()).intValue();
  }

  public SearchClient getSearchClient() {
    return searchClient;
  }

}
