package info.textgrid.rep.service;

public class ServiceConnectionException extends Exception {

  private static final long serialVersionUID = 1838320478443640813L;

  ServiceConnectionException() {
    super();
  }

  ServiceConnectionException(String s) {
    super(s);
  }

}
