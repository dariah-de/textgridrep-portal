package info.textgrid.rep.service;

import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;
import jakarta.ws.rs.ProcessingException;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AggregatorClientService {

  private static final Log log = LogFactory.getLog(AggregatorClientService.class);
  private TgrepConfigurationService tgrepConfig;

  Client restClient;

  @Autowired
  public AggregatorClientService(TgrepConfigurationService tgrepConfig) {
    this.tgrepConfig = tgrepConfig;
    restClient = ClientBuilder.newBuilder()
        .connectTimeout(this.tgrepConfig.getAggregatorConnectTimeout(), TimeUnit.SECONDS)
        .readTimeout(this.tgrepConfig.getAggregatorReadTimeout(), TimeUnit.SECONDS)
        .build()
        .property("thread.safe.client", "true")
         // use httpUrlConnection and http1, see https://cwiki.apache.org/confluence/pages/viewpage.action?pageId=49941
         .property("force.urlconnection.http.conduit", true)
        ;
  }


  public String renderTEI(String id) throws ServiceConnectionException {
    return renderTEI(id, "");
  }

  public String renderTEI(String id, String xsltUri) throws ServiceConnectionException {

    String renderedTei = "";

    WebTarget target = restClient.target(this.tgrepConfig.getTextgridHost());
    target = target.path("/1.0/aggregator/html/"+id)
        .queryParam("embedded", "true")
        .queryParam("mediatype", "text/xml");

    if(!xsltUri.equals("")) {
      log.debug("stylesheet: "+ xsltUri);
      target = target.queryParam("stylesheet", xsltUri);
    }

    try {
      Invocation.Builder builder = target.request();
      Response result = builder.get();
      renderedTei = result.readEntity(String.class);
    } catch (Exception e) {
      if (e.getCause() instanceof SocketTimeoutException) {
        throw new ServiceConnectionException(e.getCause().getMessage());
      } else {
        log.error("Exception " + e.getClass().getSimpleName() + " thrown calling renderTEI(" + id + ", " + xsltUri+"): " + e.getMessage());
      }
    } /*catch (Throwable t) {
      log.error("Throwable " + t.getClass().getSimpleName() + " thrown calling renderTEI(" + id + ", " + xsltUri+"): " + t.getMessage());
    }*/

    return renderedTei;

  }

  public String renderToc(String id) {

    String result="";

    WebTarget target = restClient.target(this.tgrepConfig.getTextgridHost());
    target = target.path("/1.0/aggregator/html/"+id)
        .queryParam("toc", "true")
        .queryParam("mediatype", "text/xml");

    try {
      Invocation.Builder builder = target.request();
      result = builder.get(String.class);
    } catch (Exception e) {
      //log.info("Error from AggregatorService", e);
      log.error("Exception " + e.getClass().getSimpleName() + " thrown calling renderToc(" + id + "): " + e.getMessage());
    } /*catch (Throwable t) {
      log.error("Throwable " + t.getClass().getSimpleName() + " thrown calling renderToc(" + id + "): " + t.getMessage());
    }*/

    return result;

  }

  public String renderTEIFragment(String id, String fragmentId) {

    String linkPattern = "";
    try {
      linkPattern = URLEncoder.encode("/browse/_/tgrep/@URI@&fragment=@ID@", "UTF-8");
    } catch (UnsupportedEncodingException e) {
      log.error("error encoding link pattern",e);
    }

    WebTarget target = restClient.target(this.tgrepConfig.getTextgridHost());
    target = target.path("/1.0/aggregator/html/"+id)
        .queryParam("embedded", "true")
        .queryParam("mediatype", "text/xml")
        .queryParam("id", fragmentId)
        .queryParam("linkPattern", linkPattern);

    Invocation.Builder builder = target.request();

    return builder.get(String.class);
  }



}
