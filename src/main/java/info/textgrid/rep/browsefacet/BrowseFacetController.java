package info.textgrid.rep.browsefacet;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import info.textgrid.namespaces.middleware.tgsearch.FacetType;
import info.textgrid.rep.service.TgrepConfigurationService;
import info.textgrid.rep.service.TgsearchClientService;

@Controller
public class BrowseFacetController {

  private TgsearchClientService tgsearchClientService;
  private TgrepConfigurationService tgrepConfig;


  private static final Log log = LogFactory.getLog(BrowseFacetController.class);

  @Autowired
  public BrowseFacetController(
      TgsearchClientService tgsearchClientService,
      TgrepConfigurationService tgrepConfig) {
    this.tgsearchClientService = tgsearchClientService;
    this.tgrepConfig = tgrepConfig;
  }

  @GetMapping("/facet/{facet}")
  public String render(
      Locale locale,
      Model model,
      @PathVariable("facet") String facet,
      @RequestParam(value="limit", required=false, defaultValue="0") int limit,
      @RequestParam(value="order", required=false, defaultValue="count.desc") String order) {

    boolean sandbox = this.tgrepConfig.getSandboxEnabled();

    // list facets configured for this portlet
    List<String> facetList = new ArrayList<String>();
    facetList.add(facet);

    List<FacetType> facets = tgsearchClientService.listFacet(facetList, limit, order, sandbox)
        .getFacetGroup().get(0).getFacet();
    model.addAttribute("facets", facets);
    model.addAttribute("facet", facet);
    return "browsefacet";
  }

}
