package info.textgrid.rep.markdown;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import info.textgrid.namespaces.middleware.tgsearch.portal.Project;
import info.textgrid.rep.i18n.I18N;
import info.textgrid.rep.i18n.I18NProvider;
import info.textgrid.rep.service.NothingFoundException;
import info.textgrid.rep.service.TgrepConfigurationService;
import info.textgrid.rep.service.TgsearchClientService;

@Controller
public class DocController {

  private ResourceLoader resourceLoader;
  private I18NProvider i18nProvider;
  private MarkdownRenderService markdownRenderService;
  private TgsearchClientService tgsearchClient;
  private TgrepConfigurationService tgrepConfig;

  private static final Log log = LogFactory.getLog(DocController.class);

  @Autowired
  public DocController(
      ResourceLoader resourceLoader,
      I18NProvider i18nProvider,
      MarkdownRenderService markdownRenderService,
      TgsearchClientService tgsearchClient,
      TgrepConfigurationService tgrepConfig) {

    this.resourceLoader = resourceLoader;
    this.i18nProvider = i18nProvider;
    this.markdownRenderService = markdownRenderService;
    this.tgsearchClient = tgsearchClient;
    this.tgrepConfig = tgrepConfig;
  }

  @GetMapping("/")
  public String renderIndex(
      Model model,
      Locale locale) {

    boolean sandbox = this.tgrepConfig.getSandboxEnabled();
    model.addAttribute("projectsPreview", true);

    // get all projects
    List<Project> projects = tgsearchClient.getProjects(sandbox).getProjects();
    model.addAttribute("projectCount", projects.size());

    // only show projects which have a portalconfig
    projects = projects.stream()
        .filter(p -> p.getPortalconfig() != null)
        .collect(Collectors.toList());

    // randomize order
    Collections.shuffle(projects);

    // only show 3 projects
    projects = projects.stream().limit(3).toList();

    model.addAttribute("projects", projects);

    return renderMarkdown(model, "index", locale);
  }

  @GetMapping("/docs/{doc}")
  public String renderMarkdown(
      Model model,
      @PathVariable("doc") String doc,
      Locale locale) {

    I18N i18n = i18nProvider.getI18N(locale);

    String rloc = "classpath:docs/" + doc + "." + i18n.getLanguage() + ".md";
    Resource res = resourceLoader.getResource(rloc);

    String content;
    try {
      content = markdownRenderService.renderHtml(res.getInputStream());
    } catch (IOException e) {
      log.error("error rendering file " + doc, e);
      throw new NothingFoundException();
    }

    model.addAttribute("content", content);

    return "markdown";

  }

}
