<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

  </main>

  <footer class="tg footer" role="contentinfo">
    <div class="tg footer_left">
      <ul>
        <li>
          <a class="tg footer_contact" href="https://textgrid.de/en/kontakt/">${i18n['contact']}</a>
        </li>
        <li><a href="https://de.dariah.eu/impressum">${i18n['imprint']}</a></li>
        <li><a href="https://de.dariah.eu/privacy-policy">${i18n['privpol']}</a></li>
        <li><a href="https://gitlab.gwdg.de/dariah-de/textgridrep-portal">${i18n['sourcecode']}</a></li>
      </ul>
      <ul>
        <li><span>&copy; TextGrid 2024</span></li>
      </ul>
    </div>
    <div class="tg footer_right">
      <div class="tg footer_logos">
        <ul>
          <li>
            <a href="https://www.tei-c.org/" target="_blank" rel="noopener">
              <img class="tg footer_logo" src="/images/tei-logo.svg" alt="TEI - Text Encoding Initiative" width="65" height="65">
            </a>
          </li>
          <li>
            <a href="https://dataverse.nl/dataset.xhtml?persistentId=doi:10.34894/L1F7BS" target="_blank" rel="noopener">
              <img class="tg footer_logo" src="/images/CoreTrustSeal-logo-transparent.png" alt="Core Trust Seal logo" width="65" height="65">
            </a>
          </li>
          <li>
            <a href="https://forschungsinfrastrukturen.de/" target="_blank" rel="noopener">
              <img class="tg footer_logo" alt="GKFI Logo" src="/images/gkfi-logo-notext.png" width="65" height="65">
            </a>
          </li>
          <li>
            <a href="https://text-plus.org/" target="_blank" rel="noopener">
              <img class="tg footer_logo" src="https://res.de.dariah.eu/logos/textplus/textplus_logo_RGB.svg" alt="Text+" width="65" height="65">
            </a>
          </li>
        </ul>
      </div>
    </div>
  </footer>

  <script src="/js/jquery.min.js"></script>
  <c:if test="${config.sentryEnabled}">
    <script src="/js/sentry.bundle.min.js"></script>
    <script src="/js/sentry.min.js"></script>
  </c:if>
  <script src="/js/main.min.js"></script>
  <c:if test="${config.trackingEnabled}">
    <script src="/js/matomo.js"></script>
  </c:if>

</div>
</body>
</html>
