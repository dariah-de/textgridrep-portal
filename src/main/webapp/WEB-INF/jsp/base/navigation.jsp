<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="tg nav -has-search" role="navigation">
  <h2 class="sr-only">${i18n['navigation']}</h2>
  <ul aria-label="${i18n['site-pages']}" role="menubar">

      <li class="tg nav_item -search">
        <form class="tg search -header" action="/search">
          <fieldset class="tg search_fake-input">
            <div class="tg search_filter">
              <c:if test="${filter.size() > 0}">
                <input class="tg search_checkbox" id="search-filters-active" type="checkbox" checked>
                <label for="search-filters-active">${i18n['filters-active']}</label>
              </c:if>
            </div>

            <div class="tg search_search">
              <label class="sr-only" for="search-query-2">${i18n['search-term']}:</label>
                <input value="${query}" class="tg search_input -header" id="search-query-2" name="query" placeholder="${i18n['search-term']}" type="search">
                <input type="hidden" name="order" value="${order}" />
                <input type="hidden" name="limit" value="${limit}" />
              <button class="tg search_submit" type="submit"><span class="sr-only">${i18n['do-search']}</span></button>
            </div>
          </fieldset>
          <a class="tg search_advanced-search-link" href="/advanced-search">${i18n['advanced-search']}</a>
        </form>
      </li>
      
      <li class="tg nav_item -has-dropdown" id="nav-explore" role="presentation">
        <a aria-labelledby="nav-explore" class="tg dropdown_toggle -nav" aria-haspopup="true" role="menuitem">
          ${i18n['content']}
        </a>
        <ul class="tg dropdown_menu -nav" role="menu">
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/projects" role="menuitem" tabindex="">${i18n['projects']}</a>
          </li>
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/facet/edition.agent.value?order=term:asc" role="menuitem" tabindex="">${i18n['by-author']}</a>
          </li>
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/facet/work.genre" role="menuitem" tabindex="">${i18n['by-genre']}</a>
          </li>
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/facet/format" role="menuitem" tabindex="">${i18n['by-filetype']}</a>
          </li>
          <!--
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/facet/project.value" role="menuitem" tabindex="">${i18n['by-project']}</a>
          </li>
          -->
        </ul>
      </li>
      
      <li class="tg nav_item -has-dropdown" id="nav-explore" role="presentation">
        <a aria-labelledby="nav-explore" class="tg dropdown_toggle -nav" aria-haspopup="true" role="menuitem">
          ${i18n['documentation']}
        </a>
        <ul class="tg dropdown_menu -nav" role="menu">
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/docs/mission-statement" role="menuitem" tabindex="">Mission Statement</a>
          </li>
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/docs/syntax" role="menuitem" tabindex="">${i18n['search']}</a>
          </li>
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/docs/shelf" role="menuitem" tabindex="">${i18n['shelf']}</a>
          </li>
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/docs/download" role="menuitem" tabindex="">Download</a>
          </li>
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/docs/voyant" role="menuitem" tabindex="">Voyant</a>
          </li>
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/docs/switchboard" role="menuitem" tabindex="">Switchboard</a>
          </li>
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/docs/annotate" role="menuitem" tabindex="">Annotate</a>
          </li>
          <li class="" id="layout_18" role="presentation">
            <a aria-labelledby="layout_18" href="/docs/errata" role="menuitem" tabindex="">Errata</a>
          </li>
        </ul>
      </li>
<!-- 
    #foreach ($nav_item in $nav_items)
      #set ($has_popup_class = '')
      #set ($toggle_popup_attr = '')
      #set ($toggle_popup_class = '')
      #set ($selected_attr = '')
      #set ($selected_class = '')

      #if ($nav_item.isSelected())
        #set ($selected_attr = 'aria-selected="true"')
        #set ($selected_class = '-current')
      #end

      #if ($nav_item.hasChildren())
        #set ($has_popup_class = '-has-dropdown')
        #set ($toggle_popup_attr = 'aria-haspopup="true"')
        #set ($toggle_popup_class = 'tg dropdown_toggle -nav')
      #end

      <li class="tg nav_item $selected_class $has_popup_class" id="layout_$nav_item.getLayoutId()" role="presentation">
        <a aria-labelledby="layout_$nav_item.getLayoutId()" href="$nav_item.getURL()" class="$toggle_popup_class" $toggle_popup_attr $nav_item.getTarget() role="menuitem">
          $nav_item.icon()
          $nav_item.getName()
        </a>

        #if ($nav_item.hasChildren())
          <ul class="tg dropdown_menu -nav" role="menu">
            #foreach ($nav_child in $nav_item.getChildren())
              #set ($child_selected_attr = '')
              #set ($child_selected_class = '')

              #if ($nav_child.isSelected())
                #set ($child_selected_attr = 'aria-selected="true"')
                #set ($child_selected_class = '-current')
              #end

              <li class="$child_selected_class" id="layout_$nav_child.getLayoutId()" $child_selected_attr role="presentation">
                <a aria-labelledby="layout_$nav_child.getLayoutId()" href="$nav_child.getURL()" $nav_child.getTarget() role="menuitem">$nav_child.getName()</a>
              </li>
            #end
          </ul>
        #end
      </li>
    #end
-->
  </ul>
</nav>
