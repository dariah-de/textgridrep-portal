<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>

<nav class="tg topbox">
  <button class="tg topbox_toggle-nav"><span class="sr-only">${i18n['navigation']}</span></button>
  <ul>
      <li>
        <a href="/shelf" class="tg topbox_link -shelf">
          ${i18n['shelf']}
          <span class="tg topbox_shelf-count">0</span>
        </a>
      </li>
<!--
      <li>
        #if ($is_signed_in)
          #set ($user = $themeDisplay.getUser())
          <a href="#search" class="tg dropdown_toggle topbox_user">$user.getFirstName() $user.getLastName()</a>
          <ul class="tg dropdown_menu">
            <li>
              <a class="tg topbox_link -settings" href="/$language/user_settings">
                #language('settings')
              </a>
            </li>
            <li>
              <a class="tg topbox_link -logout" href="$sign_out_url">
                #language('logout')
              </a>
            </li>
          </ul>
        #else
          <a class="tg topbox_link -login" href="$sign_in_url">
            ${i18n['login']}
          </a>

      </li>
-->
<!--
    <li>
        <a href="#search" class="tg dropdown_toggle topbox_user"></a>
        <ul class="tg dropdown_menu">
          <li>
            <a class="tg topbox_link -settings" href="/settings">
              ${i18n['settings']}
            </a>
          </li>
        </ul>
    </li>
-->

    <li class="tg topbox_language">
      <c:choose>
        <c:when test="${language == 'de'}">
          <a class="tg topbox_language" href="<my:replaceParam name='lang' value='en' />"><span class="sr-only">Switch language to </span>English</a>
        </c:when>
        <c:otherwise>
          <a class="tg topbox_language" href="<my:replaceParam name='lang' value='de' />"><span class="sr-only">Sprache �ndern nach </span>Deutsch</a>
        </c:otherwise>
      </c:choose>
    </li>
  </ul>
</nav>