<!DOCTYPE html>
<html lang="${language}" class="html${config.sandboxEnabled ? ' -sandbox' : '' }" prefix="og: https://ogp.me/ns#">
<head>

  <c:choose>
    <c:when test="${metadata.object.generic.provided.title[0] != null}">
      <title>${metadata.object.generic.provided.title[0]}</title>
      <c:if test="${not empty result.pathResponse.pathGroup}">
        <c:set var="path_string" value=""/>
        <c:forEach items="${result.pathResponse.pathGroup}" var="pathGroup">
          <c:forEach items="${pathGroup.path}" var="path">
            <c:forEach items="${path.entry}" var="pathEntry">
              <c:set var="path_string" value="${path_string} > ${pathEntry.title} "/>
            </c:forEach>
          </c:forEach>
        </c:forEach>
      </c:if>
      <meta name="description" content="TextGrid Repository: ${projectmap[metadata.object.generic.generated.project.id].name} ${path_string} > ${metadata.object.generic.provided.title[0]}">
      <meta property="og:description" content="TextGrid Repository: ${projectmap[metadata.object.generic.generated.project.id].name} ${path_string} > ${metadata.object.generic.provided.title[0]}">
    </c:when>
    <c:when test="${isProject}">
      <title>${project.name}</title>
      <meta name="description" content="${project.portalconfig.description.replaceAll('\\s+', ' ')}">
      <meta property="og:description" content="${project.portalconfig.description.replaceAll('\\s+', ' ')}">
    </c:when>
    <c:otherwise>
      <title>TextGrid Repository</title>
    </c:otherwise>
  </c:choose>
  <meta property="og:title" content="${metadata.object.generic.provided.title[0]}">
  <meta property="og:site_name" content="TextGrid Repository">
  <c:choose>
    <c:when test="${isProject and project.portalconfig.avatar != null}">
      <meta property="og:image" content="${config.textgridHost}/1.0/digilib/rest/IIIF/${project.portalconfig.avatar}/full/,250/0/native.jpg">
    </c:when>
    <c:when test="${image}">
      <meta property="og:image" content="${config.textgridHost}/1.0/digilib/rest/IIIF/${metadata.object.generic.generated.textgridUri.value}/full/,250/0/native.jpg">
    </c:when>
    <c:when test="${projectmap[metadata.object.generic.generated.project.id].portalconfig.avatar != null}">
      <meta property="og:image" content="${config.textgridHost}/1.0/digilib/rest/IIIF/${projectmap[metadata.object.generic.generated.project.id].portalconfig.avatar}/full/,250/0/native.jpg">
    </c:when>
    <c:otherwise>
      <meta property="og:image" content="https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:47ctb.0/full/,250/0/native.jpg">
    </c:otherwise>
  </c:choose>
  <meta property="og:image:type" content="image/jpeg" />
  <meta name="viewport" content="initial-scale=1.0, width=device-width">
  <meta charset="utf-8">
  <link rel="shortcut icon" href="/favicon.ico"/>
  <link rel="stylesheet" href="/css/custom.css?${config.cssBuildTimestamp}">
</head>

<body class="html_body aui ${pagename}">
<div class="tg site ${config.sandboxEnabled ? '-sandbox' : '' }">

  <span hidden="true" id="i18n-remove-from-shelf-label" data-value="${i18n['remove-from-shelf']}"></span>
  <span hidden="true" id="i18n-add-to-shelf-label" data-value="${i18n['add-to-shelf']}"></span>

  <header class="tg header">
    <div class="tg header_logo" role="banner">
      <a href="/" title="TextGrid Repository">
        <img src="/images/${config.sandboxEnabled ? 'textgrid-repository-sandbox-logo.svg' : 'textgrid-repository-logo.svg' }" alt="TextGrid Repository Logo">
      </a>
    </div>
    <%@ include file="topbox.jsp" %>
    <%@ include file="navigation.jsp" %>
  </header>

  <main class="tg main -default" id="content">
