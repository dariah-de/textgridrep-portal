<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils" %>

<%@ include file="base/head.jsp" %>

<div class="tgrep wrap">

  <aside class="tgrep sidebar">
    <c:if test="${viewmodes != null}">
      <section class="tgrep sidebar_panel">
        <h3 class="tgrep sidebar_subheading">${i18n['views']}</h3>
        <ul class="tgrep sidebar_list">
          <c:forEach items="${viewmodes}" var="viewmode">
            <li class="tgrep sidebar_item ${viewmode.active? '-current' : ''}">
              <a href="${viewmode.url}" rel="noindex nofollow" class="tgrep sidebar_link">${viewmode.label}</a>
            </li>
          </c:forEach>
        </ul>
        <c:if test="${viewmodes.size() > 6}">
          <button class="tgrep sidebar_expand">${i18n['expand']}</button>
        </c:if>
      </section>
    </c:if>
  </aside>

<!-- 
  <main class="tgrep main -full-width">
-->

  <main class="tgrep main">
  
    <h1>${i18n['projects']}</h1>
    <p>${i18n['projects-page-description']}</p>
  
    <div class="tgrep browse">
    
        <div class="tgrep results">
          <c:choose>
            <c:when test="${mode eq 'gallery'}">
              <ol class="tgrep results_gallery">
                <c:forEach items="${projects}" var="project">
                  <%@ include file="components/singleGalleryProject.jsp" %>
                </c:forEach>
              </ol>
            </c:when>

            <c:otherwise>
              <ol class="tgrep results_list">
                <c:forEach items="${projects}" var="project">
                  <%@ include file="components/singleListProject.jsp" %>
                </c:forEach>
              </ol>
            </c:otherwise>
          </c:choose>
        </div>
  
        <!--   
        <ul class="tgrep browse_list">
          <c:forEach items="${projects}" var="project">
            <li class="tgrep browse_item">
              <c:url context="/" value="/project/${project.id}" var="encodedUrl">
              </c:url>
              <a class="tgrep browse_link" href="${encodedUrl}">
                ${project.name}
              </a>
              (${project.count} items)
              <p>${project.portalconfig.description}</p>
            </li>
          </c:forEach>
        </ul>
        -->
      </div>

  </main>

</div>


<%@ include file="base/foot.jsp" %>
