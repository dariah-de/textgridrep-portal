<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils" %>

<c:set var="pagename" value="shelf" />
<%@ include file="base/head.jsp" %>

<span hidden="true" id="shelfItemString" data-value="${shelfItemString}"></span>

<div class="tgrep wrap">
  <div class="tgrep sidebar-toggle">
    <button class="tgrep sidebar-toggle_button -show">${i18n['show-sidebar']}</button>
    <button class="tgrep sidebar-toggle_button -hide">${i18n['hide-sidebar']}</button>
  </div>

  <aside class="tgrep sidebar">

    <c:if test="${viewmodes != null}">
      <section class="tgrep sidebar_panel">
        <h3 class="tgrep sidebar_subheading">${i18n['views']}</h3>
        <ul class="tgrep sidebar_list">
          <c:forEach items="${viewmodes}" var="viewmode">
            <li class="tgrep sidebar_item">
              <a href="${viewmode.url}" rel="noindex nofollow" class="tgrep sidebar_link">${viewmode.label}</a>
            </li>
          </c:forEach>
        </ul>
        <c:if test="${viewmodes.size() > 6}">
          <button class="tgrep sidebar_expand">${i18n['expand']}</button>
        </c:if>
      </section>
    </c:if>

    <c:if test="${results.size() > 0}">
      <section class="tgrep sidebar_panel remove_on_shelf_empty">
        <h3 class="tgrep sidebar_subheading">${i18n['tools']}</h3>
        <ul class="tgrep sidebar_list">
          <li class="tgrep sidebar_item">
            <a class="aggregator-items" href="${config.voyantHost}?input=${config.aggregatorUrl}/zip/${shelfItemString}?meta=false&only=text/xml" rel="noindex nofollow" class="tgrep sidebar_link">Voyant</a>
            <a href ="/docs/voyant" class=""><icon class="info-link"></a>
          </li>
       </ul>
       </section>
     </c:if>
  </aside>

  <main class="tgrep main">
    <h1 class="tgrep main_heading">${i18n['shelf']} <a href ="/docs/shelf"><icon class="info-link"></a></h1>

    <header class="tgrep header">
      <h2 class="tgrep header_heading">${i18n['manage-the-shelf']}</h2>
      <div class="tgrep header_info">
        ${i18n['there-are-currently']} <span class="tgrep header_count">${results.size()}</span> ${i18n['objects-on-your-shelf']}
      </div>
      <c:if test="${results.size() > 0}">
        <div class="tgrep header_actions remove_on_shelf_empty">
          <button class="tgrep header_button -expand-all">${i18n['expand-all']}</button>
          <button class="tgrep header_button -collapse-all ">${i18n['collapse-all']}</button>
          <button class="tgrep header_button -clear ">${i18n['clear']}</button>
          <div class="tg dropdown" role="group">
            <a href ="/docs/download"><icon class="info-link -large"></a> <a class="tg dropdown_toggle -download">${i18n['download-all']}</a>
            <ul class="tg dropdown_menu">
              <li class="tg dropdown_item"><a class="aggregator-items tg dropdown_link" href="${config.aggregatorUrl}/epub/${shelfItemString}" data-type="ebook">E-Book</a></li>
              <li class="tg dropdown_item"><a class="aggregator-items tg dropdown_link" href="${config.aggregatorUrl}/zip/${shelfItemString}" data-type="zip">ZIP</a></li>
              <li class="tg dropdown_item"><a class="aggregator-items tg dropdown_link" href="${config.aggregatorUrl}/zip/${shelfItemString}?transform=text&meta=false&only=text/xml&dirnames=" data-type="zip">${i18n['download-text-only-zip']}</a></li>
              <li class="tg dropdown_item"><a class="aggregator-items tg dropdown_link" href="${config.aggregatorUrl}/teicorpus/${shelfItemString}" data-type="teicorpus">TEI-Corpus</a></li>
            </ul>
          </div>
        </div>
        </c:if>
    </header>

    <c:choose>
      <c:when test="${mode eq 'gallery'}">
        <ul class="tgrep results_gallery">
          <c:forEach items="${results}" var="result">
            <%@ include file="components/singleGalleryResult.jsp" %>
          </c:forEach>
        </ul>
      </c:when>
      <c:otherwise>
        <ul class="tgrep results_list">
          <c:forEach items="${results}" var="result">
            <%@ include file="components/singleListResult.jsp" %>
          </c:forEach>
        </ul>
      </c:otherwise>
    </c:choose>
  </main>
</div>


<%@ include file="base/foot.jsp" %>

