<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils" %>

<%@ include file="base/head.jsp" %>

<div class="tgrep wrap">

  <main class="tgrep main -full-width">

    <div class="tgrep browse">
        <ul class="tgrep browse_list">
          <c:choose>

            <c:when test="${facet == 'format' or facet == 'edition.language' or facet == 'work.genre'}">
              <c:forEach items="${facets}" var="result">
                <li class="tgrep browse_item">
                  <c:url context="/" value="/search" var="encodedUrl">
                    <c:param name="filter" value="${facet}:${result.value}" />
                  </c:url>
                  <a class="tgrep browse_link" href="${encodedUrl}" title="${result.value}">
                    ${i18n[result.value] != null ? i18n[result.value] : result.value}
                  </a>
                  (${result.count} items)
                </li>
              </c:forEach>
            </c:when>

            <c:otherwise>
              <c:forEach items="${facets}" var="result">
                <li class="tgrep browse_item">
                  <c:url context="/" value="/search" var="encodedUrl">
                    <c:param name="filter" value="${facet}:${result.value}" />
                  </c:url>
                  <a class="tgrep browse_link" href="${encodedUrl}">
                    ${result.value}
                  </a>
                  (${result.count} items)
                </li>
              </c:forEach>
            </c:otherwise>

          </c:choose>
        </ul>
      </div>

  </main>

</div>


<%@ include file="base/foot.jsp" %>
