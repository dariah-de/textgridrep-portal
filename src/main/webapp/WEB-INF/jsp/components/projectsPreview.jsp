<div class="tgrep project-preview">

  <div class="journal-content-article">
    <h1>${i18n['project-preview.title']}</h1>
  </div>

  <ul class="tgrep project-preview_row">
    <c:forEach items="${projects}" var="project">

      <c:url context="/" value="/project/${project.id}" var="encodedUrl"></c:url>

      <c:choose>
        <c:when test="${project.portalconfig.avatar != null}">
          <c:url value="${config.textgridHost}/1.0/digilib/rest/IIIF/${project.portalconfig.avatar}/full/,250/0/native.jpg" var="imgUrl"></c:url>
        </c:when>
        <c:otherwise>
          <c:url context="/" value="/images/tg-logo-owl_grey_border.svg" var="imgUrl"></c:url>
        </c:otherwise>
      </c:choose>

      <c:choose>
        <c:when test="${project.portalconfig.description != null}">
          <c:set var="description" value="${project.portalconfig.description}"></c:set>
        </c:when>
        <c:otherwise>
          <c:set var="description" value="${project.name}"></c:set>
        </c:otherwise>
      </c:choose>

      <li class="tgrep project-preview_column" onclick="location.href='${encodedUrl}'">
        <div>

          <div class="tgrep project-preview_avatar">
              <a href="${encodedUrl}">
                <img src="${imgUrl}" alt="${project.name}" title="${project.name}" />
              </a>
          </div>

          <hr>

          <div class="tgrep project-preview_title">
            <h3>
            <a class="tgrep browse_link" href="${encodedUrl}">
              ${project.name}
            </a>
            </h3>
          </div>

          <div class="tgrep project-preview_description">
            <p>${description}</p>
          </div>
        </div>

      </li>

    </c:forEach>
  </ul>

  <div class="tgrep project-preview_footer">
    <p>
      <a class="tgrep button" href="/projects">${i18n['all-projects']}</a>
    </p>
  </div>

</div>
