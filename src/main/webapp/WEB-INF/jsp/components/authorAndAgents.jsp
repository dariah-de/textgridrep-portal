      <c:choose>
        <c:when test="${not empty result.object.edition.source[0].bibliographicCitation.author[0].value}">
          <dt>${i18n['author']}</dt>
          <c:set var="agent" value="${result.object.edition.source[0].bibliographicCitation.author[0]}" />
          <%@ include file="agent.jsp" %>
        </c:when>
        <c:when test="${not empty result.object.edition.agent[0].value}">
          <dt>${i18n['agent']}</dt>
          <c:forEach items="${result.object.edition.agent}" var="agent">
            <c:if test="${not empty agent.role}">
              <c:set var="agentRole">(${fn:substring(agent.role, 0, 1)}${fn:toLowerCase(fn:substring(agent.role, 1, -1))})</c:set>
            </c:if>
            <%@ include file="agent.jsp" %>
          </c:forEach>
        </c:when>
      </c:choose>