<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils"%>

<li class="tgrep gallery-item">
  <div>
    <div class="tgrep gallery-item_image">
      <c:set var="imageUrl" value="${utils:getGalleryImageUrl(config.textgridHost, result)}"/>
      <c:if test="${fn:contains(imageUrl, 'tg-icons')}">
        <c:set var="mimeIconClass" value="mime-icon" />
      </c:if>
      <a href="${utils:browseUrl(result.object.generic.generated.textgridUri.value)}?mode=gallery"> 
        <img src="${imageUrl}"
             class="${mimeIconClass}"
             alt="${result.object.generic.provided.title[0]}"
             title="${result.object.generic.provided.title[0]}" />
      </a>
    </div>
    <div class="tgrep gallery-item_title">
      <a href="${utils:browseUrl(result.object.generic.generated.textgridUri.value)}?mode=gallery"> ${result.object.generic.provided.title[0]} </a>
    </div>
  </div>
  <div class="tgrep gallery-item_actions">
    <ul>
      <li><c:choose>
          <c:when test="${shelf.contains(result.object.generic.generated.textgridUri.value)}">
            <button class="tgrep gallery-item_button -remove-from-shelf" data-textgrid-targeturi="${result.object.generic.generated.textgridUri.value}">
              ${i18n['remove-from-shelf']}</button>
          </c:when>
          <c:otherwise>
            <button class="tgrep gallery-item_button -add-to-shelf" data-textgrid-targeturi="${result.object.generic.generated.textgridUri.value}">
              ${i18n['add-to-shelf']}</button>
          </c:otherwise>
        </c:choose></li>
      <li><a href="${config.textgridHost}/1.0/tgcrud-public/rest/${result.object.generic.generated.textgridUri.value}/data"
        class="tgrep gallery-item_button -download">${i18n['download']}</a></li>
    </ul>
  </div>
</li>
