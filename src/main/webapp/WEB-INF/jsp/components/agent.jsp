<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="agentString" value="${agent.value}"></c:set>
<dd>
  <c:choose>
    <c:when test="${not empty agent.id}">
      <c:choose>
        <c:when test="${fn:startsWith(agent.id, 'pnd:')}">
          <a href="https://d-nb.info/gnd/${fn:substringAfter(agent.id, 'pnd:')}">${agentString}</a> ${agentRole}
        </c:when>
        <c:when test="${fn:startsWith(agent.id, 'gnd:')}">
          <a href="https://d-nb.info/gnd/${fn:substringAfter(agent.id, 'gnd:')}">${agentString}</a> ${agentRole}
        </c:when>
        <c:when test="${fn:startsWith(agent.id, 'viaf:')}">
          <a href="https://viaf.org/viaf/${fn:substringAfter(agent.id, 'viaf:')}">${agentString}</a> ${agentRole}
        </c:when>
        <c:otherwise>
          <a href="${agent.id}">${agentString}</a> ${agentRole} 
        </c:otherwise>
      </c:choose>
    </c:when>
    <c:otherwise>
      ${agentString} ${agentRole}
    </c:otherwise>
  </c:choose>
</dd>
