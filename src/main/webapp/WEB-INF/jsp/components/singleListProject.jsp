<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils" %>

<c:url context="/" value="/project/${project.id}" var="encodedUrl"></c:url>

<li class="tgrep result">
  <div class="tgrep result_main -full-width -blockalign">

    <c:if test="${project.portalconfig.avatar != null}">
    <div class="tgrep result_image -float-right">
        <a class="tgrep browse_link" href="${encodedUrl}">
           <img src="${config.textgridHost}/1.0/digilib/rest/IIIF/${project.portalconfig.avatar}/full/,250/0/native.jpg" alt="${project.name}" title="${project.name}" />
        </a>
    </div>
    <span class="clearboth"></span>
    </c:if>

    <div class="tgrep result_title">

      <a class="tgrep browse_link" href="${encodedUrl}">
        ${project.name}
      </a>
    </div>

    <ol class="tgrep metadata_list">
      <c:if test="${project.portalconfig.description != null}">
        <li>${project.portalconfig.description}</li>
      </c:if>
      <li><strong>${project.count} ${i18n['objects']}</strong></li>
    </ol>
    <span class="clearboth"></span>
  </div>

</li>
