<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils" %>

<li class="tgrep result">
  <div class="tgrep result_main" data-tg-uri="${result.object.generic.generated.textgridUri.value}">
    <div class="tgrep result_author">
      <c:choose>
        <c:when test="${not empty result.author}">
          ${result.author}
        </c:when>
        <c:when test="${not empty result.object.edition.agent[0].value}">
          ${result.object.edition.agent[0].value}
        </c:when>
        <c:when test="${not empty result.object.work.agent[0].value}">
          ${result.object.work.agent[0].value}
        </c:when>
      </c:choose>
    </div>
    <div class="tgrep result_title">
      <a href="${utils:browseUrl(result.object.generic.generated.textgridUri.value)}">
        ${result.object.generic.provided.title[0]}
      </a>
    </div>
    <div class="tgrep result_info">
      <div title="${result.object.generic.provided.format}" class="tgrep result_mime-type -${utils:replaceAll(result.object.generic.provided.format, '\\W', '-')}">
        <span class="sr-only">${result.object.generic.provided.format}</span>
      </div>
      <div class="tgrep result_breadcrumbs">
        <%@ include file="path.jsp" %>
      </div>
    </div>

    <ol class="tgrep metadata_list">
      <c:forEach items="${result.fulltext}" var="fulltext">
        <c:forEach items="${fulltext.kwic}" var="kwic">
          <li class="tgrep metadata_item">${kwic.left} <em>${kwic.match}</em> ${kwic.right}
          <!--  <a href="#">show</a></li> -->
          <c:if test="${not empty kwic.refTitle}">
            <a href="${utils:browseUrl(result.object.generic.generated.textgridUri.value)}?fragment=${kwic.refId}">${kwic.refTitle}</a>
          </c:if>
        </c:forEach>
      </c:forEach>
    </ol>
  </div>

  <div class="tgrep result_image">
    <a href="${utils:browseUrl(result.object.generic.generated.textgridUri.value)}">
      <img src="${utils:getImageUrl(config.textgridHost, result)}" alt="${result.object.generic.provided.title[0]}" title="${result.object.generic.provided.title[0]}" />
    </a>
  </div>

  <div class="tgrep result_actions">
    <ul>
      <li>
        <c:choose>
          <c:when test="${shelf.contains(result.object.generic.generated.textgridUri.value)}">
            <button
              class="tgrep result_button -remove-from-shelf"
              data-textgrid-targeturi="${result.object.generic.generated.textgridUri.value}">
              ${i18n['remove-from-shelf']}
            </button>
          </c:when>
          <c:otherwise>
            <button
              class="tgrep result_button -add-to-shelf"
              data-textgrid-targeturi="${result.object.generic.generated.textgridUri.value}">
              ${i18n['add-to-shelf']}
            </button>
          </c:otherwise>
        </c:choose>
      </li>
      <li>
        <a href="${config.textgridHost}/1.0/tgcrud-public/rest/${result.object.generic.generated.textgridUri.value}/data" class="tgrep result_button -download">${i18n['download']}</a>
      </li>
    </ul>
  </div>

  <div class="tgrep metadata">
    <!-- TODO: each for agent, pid, etc -->
    <button class="tgrep metadata_button -show">${i18n['more-metadata']}</button>
    <button class="tgrep metadata_button -hide">${i18n['less-metadata']}</button>
    <dl class="tgrep metadata_toggle">
      <dt>${i18n['format']}</dt>
      <dd>${result.object.generic.provided.format}</dd>
      <!-- show author when available, first agent otherwise -->
      <%@ include file="authorAndAgents.jsp" %>
      <c:if test="${not empty result.object.edition.source[0].bibliographicCitation.dateOfPublication.date}">
        <dt>${i18n['date-of-publication']}</dt>
        <dd>${result.object.edition.source[0].bibliographicCitation.dateOfPublication.date}</dd>
      </c:if>
      <c:if test="${not empty result.object.edition.source[0].bibliographicCitation.placeOfPublication[0].value}">
        <dt>${i18n['place-of-publication']}</dt>
        <dd>${result.object.edition.source[0].bibliographicCitation.placeOfPublication[0].value}</dd>
      </c:if>
      <c:if test="${not empty result.object.generic.provided.notes}">
        <dt>${i18n['notes']}</dt>
        <dd>${result.object.generic.provided.notes}</dd>
      </c:if>
      <c:if test="${not empty result.object.generic.generated.pid[0].value}">
        <dt>${i18n['pid']}</dt>
        <dd>
          ${result.object.generic.generated.pid[0].value}
          (<a href="${config.handleHost}/${result.object.generic.generated.pid[0].value}">resolve PID</a> /
          <a href="${config.handleHost}/${result.object.generic.generated.pid[0].value}?noredirect">PID metadata</a>)
        </dd>
      </c:if>
      <dt>${i18n['revision']}</dt>
      <dd>${result.object.generic.generated.revision}</dd>
      <dt>${i18n['raw-metadata']}</dt>
      <dd><a href="${config.textgridHost}/1.0/tgcrud-public/rest/${result.object.generic.generated.textgridUri.value}/metadata">XML</a></dd>
      <dt>${i18n['tech-metadata']}</dt>
      <dd><a href="${config.textgridHost}/1.0/tgcrud-public/rest/${result.object.generic.generated.textgridUri.value}/tech">XML</a></dd>
    </dl>
  </div>
</li>
