<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils" %>

<c:if test="${mode =='gallery'}">
  <c:set var="modestring" value="?mode=${mode}" />
</c:if>

<ul class="tgrep breadcrumbs">
  <c:if test="${not empty result.pathResponse.pathGroup}">
    <li class="tgrep breadcrumbs_item">
      <a class="tgrep breadcrumbs_link" href="/project/${result.object.generic.generated.project.id}">
        ${projectmap[result.object.generic.generated.project.id].name}
      </a>
    </li>
  </c:if>
  <c:forEach items="${result.pathResponse.pathGroup}" var="pathGroup">
    <c:forEach items="${pathGroup.path}" var="path">
      <c:forEach items="${path.entry}" var="pathEntry">
        <li class="tgrep breadcrumbs_item">
          <a class="tgrep breadcrumbs_link" href="${utils:browseUrl(pathEntry.textgridUri)}${modestring}">
            ${pathEntry.title}
          </a>
        </li>
      </c:forEach>
    </c:forEach>
  </c:forEach>
  <li class="tgrep breadcrumbs_item">
    <a class="tgrep breadcrumbs_link" href="${utils:browseUrl(result.object.generic.generated.textgridUri.value)}${modestring}">
      ${result.object.generic.provided.title[0]}
    </a>
  </li>
</ul>
