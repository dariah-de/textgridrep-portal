<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils"%>

<c:url context="/" value="/project/${project.id}" var="encodedUrl"></c:url>

<c:choose>
  <c:when test="${project.portalconfig.avatar != null}">
    <c:url value="${config.textgridHost}/1.0/digilib/rest/IIIF/${project.portalconfig.avatar}/full/,500/0/native.jpg" var="imgUrl"></c:url>
  </c:when>
  <c:otherwise>
    <c:url context="/" value="/static/images/no_image.svg" var="imgUrl"></c:url>
  </c:otherwise>
</c:choose>

<c:choose>
  <c:when test="${project.portalconfig.description != null}">
    <c:set var="description" value="${project.portalconfig.description}"></c:set>
  </c:when>
  <c:otherwise>
     <c:set var="description" value="${project.name}"></c:set>
  </c:otherwise>
</c:choose>

<c:set var="description" value="${description} / ${project.count} objects"></c:set>

<li class="tgrep gallery-item">
  <div>
    <div class="tgrep gallery-item_image"">
        <a href="${encodedUrl}?mode=gallery">
           <img src="${imgUrl}" alt="${project.name}" title="${description}" />
        </a>
    </div>
    <div class="tgrep gallery-item_title">
      <a class="tgrep browse_link" href="${encodedUrl}?mode=gallery">
        ${project.name}
      </a>
    </div>
  </div>

</li>
