<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${pager.isLastPageButHasMoreHits}">
  <hr>
  <h2>${i18n['last-page-header']}</h2>
  <p>
    ${i18n['last-page-message-1']} <strong>${pager.maxHits}</strong> ${i18n['of-total']} ${pager.hits} ${i18n['last-page-message-2']}<br>
    ${i18n['last-page-message-3']}<br>
    ${i18n['last-page-message-4']}<br>
    ${i18n['last-page-message-5']} <a href="/docs/syntax">${i18n['search-doc-link']}</a>.
  </p>
</c:if>

<div class="tg pagination">
  <ul class="tg pagination_list">
    <!-- previous button -->
    <c:if test="${pager.start eq 0}">
      <li class="tg pagination_item -disabled">
        <span class="tg pagination_link -first" title="${i18n['go-to-first-page']}">
          <span class="sr-only">${i18n['first-page']}</span>
        </span>
      </li>

      <li class="tg pagination_item -disabled">
        <span class="tg pagination_link -prev" title="${i18n['go-to-previous-page']}">
          <span class="sr-only">${i18n['previous-page']}</span>
        </span>
      </li>
    </c:if>

      <c:if test="${pager.start gt 0}">
      <li class="tg pagination_item">
        <a class="tg pagination_link -first" href="?query=${query}${filterQueryString}&start=0&limit=${pager.limit}&order=${order}&mode=${mode}" title="${i18n['go-to-first-page']}">
          <span class="sr-only">${i18n['first-page']}</span>
        </a>
      </li>

      <li class="tg pagination_item">
        <!-- previous start shall be 0 if limit is bigger than start, else (start - limit), to prevent negative values (may occur when changing limits), see https://gitlab.gwdg.de/dariah-de/textgridrep-portal/-/issues/232 -->
        <a class="tg pagination_link -prev" href="?query=${query}${filterQueryString}&start=${pager.start < pager.limit ? 0 : pager.start - pager.limit}&limit=${pager.limit}&order=${order}&mode=${mode}" title="${i18n['go-to-previous-page']}">
          <span class="sr-only">${i18n['previous-page']}</span>
        </a>
      </li>
    </c:if>


    <!-- pages -->
    <c:forEach items="${pager.pages}" var="page">
      <c:if test="${(page - 1) * pager.limit != pager.start}">
        <li class="tg pagination_item">
          <a class="tg pagination_link" href="?query=${query}${filterQueryString}&start=${(page - 1) * pager.limit}&limit=${pager.limit}&order=${order}&mode=${mode}" title="${i18n['go-to-page']} ${page}">
            <span class="sr-only">${i18n['page']}</span> ${page}
          </a>
        </li>
      </c:if>

      <c:if test="${(page - 1) * pager.limit == pager.start}">
        <li class="tg pagination_item -current">
          <a class="tg pagination_link" href="?query=${query}${filterQueryString}&start=${(page - 1) * limit}&limit=${pager.limit}&order=${order}&mode=${mode}" title="${i18n['go-to-page']} ${page}">
            <span class="sr-only">${i18n['page']}</span> ${page}
          </a>
        </li>
      </c:if>

    </c:forEach>

    <!-- next & last page button clickable -->
    <c:if test="${((pager.start + pager.limit) lt pager.hits) and not pager.isLastPageButHasMoreHits}">
      <li class="tg pagination_item">
        <a class="tg pagination_link -next" href="?query=${query}${filterQueryString}&start=${pager.start + pager.limit}&limit=${pager.limit}&order=${order}&mode=${mode}" title="${i18n['go-to-next-page']}">
          <span class="sr-only">${i18n['next-page']}</span>
        </a>
      </li>
      <li class="tg pagination_item">
        <a class="tg pagination_link -last" href="?query=${query}${filterQueryString}&start=${(pager.totalPages-1) * pager.limit}&limit=${pager.limit}&order=${order}&mode=${mode}" title="${i18n['go-to-last-page']}">
          <span class="sr-only">${i18n['last-page']}</span>
        </a>
      </li>
    </c:if>

    <!-- next & last page button disabled -->
    <c:if test="${((pager.start + pager.limit) ge pager.hits) or pager.isLastPageButHasMoreHits}">
      <li class="tg pagination_item -disabled">
        <span class="tg pagination_link -next" title="${i18n['go-to-next-page']}">
          <span class="sr-only">${i18n['next-page']}</span>
        </span>
      </li>
      <li class="tg pagination_item -disabled">
        <span class="tg pagination_link -last" title="${i18n['go-to-last-page']}">
          <span class="sr-only">${i18n['last-page']}</span>
        </span>
      </li>
    </c:if>

  </ul>
</div>
