<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ include file="base/head.jsp"%>


<img src="/images/book.svg" alt="reading owl">
<h2>Sorry, tried to look it up - but found nothing here.</h2>
<a href="/">Back to main page</a>

<%@ include file="base/foot.jsp"%>