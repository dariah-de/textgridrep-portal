<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils" %>

<c:set var="pagename" value="browse" />
<%@ include file="base/head.jsp" %>

<div class="tgrep wrap">

  <c:if test="${browseRootAggregations}">
    <aside class="tgrep sidebar">
      <c:if test="${viewmodes != null}">
        <section class="tgrep sidebar_panel">
          <h3 class="tgrep sidebar_subheading">${i18n['views']}</h3>
          <ul class="tgrep sidebar_list">
            <c:forEach items="${viewmodes}" var="viewmode">
              <li class="tgrep sidebar_item ${viewmode.active? '-current' : ''}">
                <a href="${viewmode.url}" rel="noindex nofollow" class="tgrep sidebar_link">${viewmode.label}</a>
              </li>
            </c:forEach>
          </ul>
          <c:if test="${viewmodes.size() > 6}">
            <button class="tgrep sidebar_expand">${i18n['expand']}</button>
          </c:if>
        </section>
      </c:if>
    </aside>
  </c:if>

  <c:if test="${! browseRootAggregations}">
    <div class="tgrep sidebar-toggle">
      <button class="tgrep sidebar-toggle_button -show">${i18n['show-sidebar']}</button>
      <button class="tgrep sidebar-toggle_button -hide">${i18n['hide-sidebar']}</button>
    </div>

    <aside class="tgrep sidebar">

      <c:if test="${not isProject}">
        <section class="tgrep sidebar_panel">
          <h3 class="tgrep sidebar_subheading">${i18n['metadata']}</h3>
          <!-- TODO: each for agent, pid, etc -->
          <dl>
            <dt>${i18n['format']}</dt><dd>${metadata.object.generic.provided.format}</dd>

            <c:if test="${not empty metadata.object.edition.isEditionOf}">
            <dt>${i18n['related-work']}</dt><dd><a href="/browse/${metadata.object.edition.isEditionOf}">${metadata.object.edition.isEditionOf}</a></dd>
            </c:if>

            <!-- show author when available, first agent otherwise -->
            <%@ include file="components/authorAndAgents.jsp" %>

            <c:if test="${not empty metadata.object.edition.source[0].bibliographicCitation.dateOfPublication.date}">
              <dt>${i18n['date-of-publication']}</dt><dd>${metadata.object.edition.source[0].bibliographicCitation.dateOfPublication.date}</dd>
            </c:if>
            <c:if test="${not empty metadata.object.edition.source[0].bibliographicCitation.placeOfPublication[0].value}">
              <dt>${i18n['place-of-publication']}</dt><dd>${metadata.object.edition.source[0].bibliographicCitation.placeOfPublication[0].value}</dd>
            </c:if>
            <c:if test="${not empty metadata.object.generic.generated.pid[0].value}">
              <dt>${i18n['pid']}</dt>
                          <dd><a href="${config.handleHost}/${fn:substringAfter(metadata.object.generic.generated.pid[0].value, 'hdl:')}">${metadata.object.generic.generated.pid[0].value}</a><br/>
                          <a href="#citation">${i18n['citation']}</a></dd>
            </c:if>
          </dl>
        </section>
      </c:if>


      <c:if test="${revisions.size() > 1}">
        <section class="tgrep sidebar_panel">
          <h3 class="tgrep sidebar_subheading">${i18n['revisions']}</h3>
          <ul class="tgrep sidebar_list">
            <c:forEach items="${revisions}" var="rev">
                <li class="tgrep sidebar_item ${rev.key == metadata.object.generic.generated.revision ? '-current' : ''}">
                  <a href="/browse/${rev.value}">${rev.key}</a>
                </li>
            </c:forEach>
          </ul>
          <c:if test="${revisions.size() > 6}">
            <button class="tgrep sidebar_expand">${i18n['expand']}</button>
          </c:if>
        </section>
      </c:if>

      <c:if test="${not isProject}">

        <c:if test="${projectXsltUri != '' and teiHtml != null}">
          <section class="tgrep sidebar_panel">
            <h3 class="tgrep sidebar_subheading">${i18n['project-xslt-used']}</h3>
            <ul class="tgrep sidebar_list">
                <li class="tgrep sidebar_item">
                  <a href="/browse/${projectXsltUri}">${projectXsltUri}</a>
                </li>
            </ul>
          </section>
        </c:if>

        <section class="tgrep sidebar_panel">
          <h3 class="tgrep sidebar_subheading">${i18n['download']}</h3>
          <ul class="tgrep sidebar_list">
            <li>
              <a href="${config.textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/data">
                ${i18n['object']} <c:if test="${isTEI}">(TEI)</c:if>
              </a>
            </li>
                      <li>
              <a href="${config.textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/metadata">
                ${i18n['metadata']} (XML)
              </a>
            </li>
                      <li>
              <a href="${config.textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/tech">
                ${i18n['techmd']} (XML)
              </a>
            </li>
            <c:if test="${isTEI}">
              <li>
                <a href="${config.textgridHost}/1.0/aggregator/text/${metadata.object.generic.generated.textgridUri.value}">
                  Plain Text (txt)
                </a>
              </li>
              <li>
                <a href="${config.textgridHost}/1.0/aggregator/epub/${metadata.object.generic.generated.textgridUri.value}">
                  E-Book (epub)
                </a>
              </li>
              <li>
                <a href="${config.textgridHost}/1.0/aggregator/html/${metadata.object.generic.generated.textgridUri.value}">
                  HTML
                </a>
              </li>
              <li>
                <a href="${config.textgridHost}/1.0/aggregator/zip/${metadata.object.generic.generated.textgridUri.value}">
                  ZIP
                </a>
              </li>
            </c:if>
            <c:if test="${fn:contains(metadata.object.generic.provided.format, 'aggregation')}">
              <a href="${config.textgridHost}/1.0/aggregator/teicorpus/${metadata.object.generic.generated.textgridUri.value}">
                TEI-Corpus (XML)
              </a>
            </c:if>
          </ul>
        </section>
      </c:if>

      <c:if test="${viewmodes != null}">
        <section class="tgrep sidebar_panel">
          <h3 class="tgrep sidebar_subheading">${i18n['views']}</h3>
          <ul class="tgrep sidebar_list">
            <c:forEach items="${viewmodes}" var="viewmode">
              <li class="tgrep sidebar_item ${viewmode.active? '-current' : ''}">
                <a href="${viewmode.url}" rel="noindex nofollow" class="tgrep sidebar_link">${viewmode.label}</a>
              </li>
            </c:forEach>
          </ul>
          <c:if test="${viewmodes.size() > 6}">
            <button class="tgrep sidebar_expand">${i18n['expand']}</button>
          </c:if>
        </section>
      </c:if>

      <c:if test="${tools != null and tools.size() > 0}">
        <section class="tgrep sidebar_panel">
          <h3 class="tgrep sidebar_subheading">${i18n['tools']}</h3>
          <ul class="tgrep sidebar_list">
            <c:forEach items="${tools}" var="tool">
              <li class="tgrep sidebar_item">
                <a href="${tool.url}" rel="noindex nofollow" class="tgrep sidebar_link ${tool.cssClass}">${tool.label}</a>
                <c:if test="${tool.helpLink != null}">
                  <a href ="${tool.helpLink}" class=""><icon class="info-link"></a>
                </c:if>
              </li>
            </c:forEach>
          </ul>
          <c:if test="${tools.size() > 6}">
            <button class="tgrep sidebar_expand">${i18n['expand']}</button>
          </c:if>
        </section>
      </c:if>

      <c:if test="${tocHtml != null}">
        <section class="tgrep sidebar_panel">
          <h3 class="tgrep sidebar_subheading">${i18n['toc']}</h3>
            ${tocHtml}
        </section>
      </c:if>

    </aside>
  </c:if>

  <main class="tgrep main">
  
    <c:if test="${higherRevisionAvailable}">
      <fieldset class="tgrep advanced-search_fieldset">
        <legend class="tgrep advanced-search_legend">${i18n['higher-revision-available']}</legend>
        ${i18n['higher-rev-msg-pt1']} ${metadata.object.generic.generated.revision} ${i18n['higher-rev-msg-pt2']}
        <a href="/browse/${latestRevisionUri}?mode=${mode}">Revision ${latestRevision}</a>.
      </fieldset>
    </c:if>

    <c:if test="${metadata.object.generic.provided.format eq 'application/xml;derived=true'}">
        <fieldset class="tgrep advanced-search_fieldset">
          <legend class="tgrep advanced-search_legend">${i18n['derived-text-header']}</legend>
          ${i18n['derived-text-message']}
        </fieldset>
    </c:if>

    <%@ include file="components/path.jsp" %>
    
    <c:if test="${isProject}">
        <c:if test="${project.portalconfig.avatar != null}">
          <img class="avatar" src="${config.textgridHost}/1.0/digilib/rest/IIIF/${project.portalconfig.avatar}/full/,250/0/native.jpg" alt="${project.name}" title="${project.name}" />
        </c:if>
        <h1>${project.name}</h1>
        <p>${project.portalconfig.description} <c:if test="${readme != null}"><a href="#README">[${i18n['more']}...]</a></c:if></p>
        <div class="clearboth"></div>
    </c:if>

    <c:choose>
      <c:when test="${results != null}">

        <c:if test="${isProject}">
          <div class="tgrep header_info">
            Aggregation <span class="tgrep header_count -current">${pager.start + 1}&#8211;${pager.end}</span> ${i18n['of']}
            <span class="tgrep header_count -total">${pager.hits}</span>

            <div class="tg dropdown" role="group">
              <a class="tg dropdown_toggle -settings">${i18n['change-result-display']}</a>
              <ul class="tg dropdown_menu">
                <li class="tg dropdown_item">
                  <span class="tg dropdown_heading">${i18n['results-per-page']}</span>
                  <ul class="tg dropdown_submenu">
                    <li class="tg dropdown_item  ${limit eq '10' ? '-current' : ''}">
                      <a href="?query=${query}${filterQueryString}&order=${order}&start=${start}&mode=${mode}&limit=10">10</a>
                    </li>
                    <li class="tg dropdown_item ${limit eq '20' ? '-current' : ''}">
                      <a href="?query=${query}${filterQueryString}&order=${order}&start=${start}&mode=${mode}&limit=20">20</a>
                    </li>
                    <li class="tg dropdown_item ${limit eq '50' ? '-current' : ''}">
                      <a href="?query=${query}${filterQueryString}&order=${order}&start=${start}&mode=${mode}&limit=50">50</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </c:if>

        <c:if test="${format == 'text/tg.work+xml'}">
          <h1 class="tgrep main_heading">${i18n['editions-for-this-work']}</h1>
        </c:if>

        <div class="tgrep results">
          <c:choose>
            <c:when test="${mode eq 'gallery'}">
              <ol class="tgrep results_gallery">
                <c:forEach items="${results}" var="result">
                  <%@ include file="components/singleGalleryResult.jsp" %>
                </c:forEach>
              </ol>
              <div class="clearboth"></div>
            </c:when>

            <c:otherwise>
              <ol class="tgrep results_list">
                <c:forEach items="${results}" var="result">
                  <%@ include file="components/singleListResult.jsp" %>
                </c:forEach>
              </ol>
            </c:otherwise>
          </c:choose>

          <c:if test="${isProject}">
            <footer class="tgrep footer">
              <%@ include file="components/pager.jsp" %>
            </footer>
          </c:if>

        </div>
      </c:when>

      
      <c:when test="${isRenderingSlow}">
      
        <img src="/images/wanne.svg" class="floatright" style="width: 40%"></img>
        <h1>${i18n['slow-transform-head']}</h1>
        <p>
          ${i18n['slow-transform-pt1']} ${renderingTimeout} ${i18n['slow-transform-pt2']} 
        </p><p>
          ${i18n['slow-transform-pt3']}
        <p>
        <ul>
          <li>
            <a target="_blank" href="${config.textgridHost}/1.0/aggregator/html/${metadata.object.generic.generated.textgridUri.value}?mediatype=text/xml">
              ${i18n['open-aggregator']}
            </a>
          </li>
          <li>
            <a target="_blank" href="${config.textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/data">
              ${i18n['download-xml']}
            </a>
          </li>
        </ul>
<!-- 
        <dl>
          <dt>Timeout in seconds</dt>
          <dd>${renderingTimeout}</dd>
          <dt>ErrorMessage</dt>
          <dd>${renderingMessage}</dd>
        </dl>
-->          
      </c:when>

      <c:when test="${teiHtml != null}">
        <div class="teiXsltView">
          ${teiHtml}
        </div>
      </c:when>

      <c:when test="${image}">
       <!-- TODO: openseadragon without inline javscript 
        <div id="openseadragon1" style="width: 800px; height: 1200px;"></div>
        <script src="/js/openseadragon.min.js"></script>
        <script type="text/javascript">
          var viewer = OpenSeadragon({
              id: "openseadragon1",
              preserveViewport: true,
              visibilityRatio:    1,
              minZoomLevel:       1,
              defaultZoomLevel:   1,
              //sequenceMode:       true,
              prefixUrl: "/images/openseadragon/",
              tileSources: "${config.textgridHost}/1.0/digilib/rest/IIIF/${metadata.object.generic.generated.textgridUri.value}/info.json"
          });
       </script>
       -->
        <img class="main-image-content" src="${config.textgridHost}/1.0/digilib/rest/IIIF/${metadata.object.generic.generated.textgridUri.value}/full/,1000/0/native.jpg"/>
      </c:when>

      <c:when test="${metadata.object.generic.provided.identifier[0].type eq 'METSXMLID'}">
        ${i18n['dfg-viewer-mets-message']}
      </c:when>

      <c:when test="${metadata.object.generic.provided.format eq 'text/html'}">
        <p>
          ${i18n['iframe-html-message']}
        </p>
        <a target="_blank" href="${config.textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/data">${i18n['open-html-in-new-window']}</a>
        <iframe id="htmlIframe" width="560" height="600" frameborder="0" src="${config.textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/data"></iframe>
      </c:when>

      <c:when test="${metadata.object.generic.provided.format eq 'text/markdown'}">
        <div class="journal-content-article markdown-doc">
          ${htmlContent}
        </div>
      </c:when>

      <c:when test="${metadata.object.generic.provided.format eq 'text/plain'}">
        <pre>
          ${textContent}
        </pre>
      </c:when>

      <c:when test="${fn:startsWith(metadata.object.generic.provided.format, 'audio/')}">
        <audio controls>
          <source src="${config.textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/data" type="${metadata.object.generic.provided.format}">
        </audio>
      </c:when>

      <c:otherwise>
        <p>${i18n['dont-know-what-todo']} ${format}</p>
        <a target="_blank" href="${config.textgridHost}/1.0/tgcrud-public/rest/${metadata.object.generic.generated.textgridUri.value}/data">${i18n['open-html-in-new-window']}</a>
      </c:otherwise>

    </c:choose>

    <!-- Notes and License (e.g. for editions) -->
    <c:if test="${not empty metadata.object.edition.license.value
                  or not empty metadata.object.generic.provided.notes
                  or not empty metadata.object.item.rightsHolder[0].value}">
      <div class="clearboth">
      <hr/>
      <dl>
        <c:if test="${not empty metadata.object.generic.provided.notes}">
          <dt>${i18n['notes']}</dt><dd>${metadata.object.generic.provided.notes}</dd>
        </c:if>
        <c:if test="${not empty metadata.object.item.rightsHolder[0].value}">
          <dt>${i18n['rightsholder']}</dt><dd>${metadata.object.item.rightsHolder[0].value}</dd>
        </c:if>
        <c:if test="${not empty metadata.object.edition.license.value}">
          <dt>${i18n['license']}</dt>
            <dd>
              ${metadata.object.edition.license.value}
              <c:if test="${not empty metadata.object.edition.license.licenseUri}">
                <br/><a href="${metadata.object.edition.license.licenseUri}">${i18n['licenseUri']}</a>
              </c:if>
            </dd>
        </c:if>
      </dl>
      </div>
    </c:if>

    <c:if test="${not empty readme}">
    <hr id="README">
      <div class="journal-content-article markdown-doc">
        ${readme}
      </div>
    </c:if>

    <c:if test="${not isProject}">
      <!-- Citation examples -->
      <div id="citation" class="clearboth">
      <hr/>
      <%@ include file="components/citation.jsp" %>
    </c:if>
  </main>
</div>

<%@ include file="base/foot.jsp" %>
