<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ include file="base/head.jsp"%>

<c:if test="${projectsPreview}">
  <%@ include file="components/projectsPreview.jsp"%>
</c:if>

<div class="journal-content-article markdown-doc">${content}</div>

<%@ include file="base/foot.jsp"%>
