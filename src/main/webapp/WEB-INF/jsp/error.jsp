<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ include file="base/head.jsp"%>


<img src="/images/drunk.svg" alt="drunk owl" class="drunkenowl">
<h2>Ups, an error occured</h2>

<div>
  Status code: <b>${statusCode}</b>
</div>
<div>
  Exception Message: <b>${exception}</b>
</div>

<span class="clearboth"></span>

<pre>
  <c:forEach items="${stacktrace}" var="stelem">
     ${stelem}
  </c:forEach>
</pre>

<%@ include file="base/foot.jsp"%>