<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://textgrid.info/rep/utils" prefix="utils" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="pagename" value="search" />
<%@ include file="base/head.jsp" %>

<div class="tgrep wrap">

  <c:if test="${results.size() eq 0 and not empty query and empty filter}">
    <main class="tgrep main">

       ${i18n['nothing-found']} <em>${query}</em>

    </main>
  </c:if>

  <c:if test="${results.size() > 0 or not empty filter}">

    <div class="tgrep sidebar-toggle">
      <button class="tgrep sidebar-toggle_button -show">${i18n['show-sidebar']}</button>
      <button class="tgrep sidebar-toggle_button -hide">${i18n['hide-sidebar']}</button>
    </div>

    <aside class="tgrep sidebar -filter">
      <c:if test="${viewmodes != null}">
        <section class="tgrep sidebar_panel">
          <h3 class="tgrep sidebar_subheading">${i18n['views']}</h3>
          <ul class="tgrep sidebar_list">
            <c:forEach items="${viewmodes}" var="viewmode">
              <li class="tgrep sidebar_item ${viewmode.active? '-current' : ''}">
                <a href="${viewmode.url}" rel="noindex nofollow" class="tgrep sidebar_link">${viewmode.label}</a>
              </li>
            </c:forEach>
          </ul>
          <c:if test="${viewmodes.size() > 6}">
            <button class="tgrep sidebar_expand">${i18n['expand']}</button>
          </c:if>
        </section>
      </c:if>

      <h2 class="tgrep sidebar_heading">${i18n['filter']}</h2>

      <c:if test="${filter.size() > 0}">
        <section class="tgrep sidebar_panel">
          <h3 class="tgrep sidebar_subheading">${i18n['active-filters']}</h3>
            <ul class="tgrep sidebar_list">
              <c:forEach items="${filter}" var="activeFilter">
                <c:set var="afArray" value="${fn:split(activeFilter, ':')}" />
                <li class="tgrep sidebar_item -filter" data-filter="${activeFilter}">
                  <a class="tgrep sidebar_remove-filter"
                      title="${i18n['remove-filter']}"
                      href="?query=${query}&order=${order}&limit=${limit}&mode=${mode}${fn:replace(filterQueryString, '&filter='.concat(afArray[0]).concat('%3A').concat(utils:urlencode(afArray[1])) , '')}">
                    <span class="sr-only">${i18n['remove-filter']}</span>
                  </a>
                  <c:choose>
                    <c:when test="${afArray[0] == 'project.id'}">
                      <c:set var="facetvalue" value="${projectmap[afArray[1]].name}" />
                    </c:when>
                    <c:otherwise>
                      <c:set var="facetvalue" value="${afArray[1]}" />
                    </c:otherwise>
                </c:choose>
                  <strong>${i18n[afArray[0]]}</strong>: ${facetvalue}
                </li>
              </c:forEach>
            </ul>
            <c:if test="${filter.size() > 6}">
              <button class="tgrep sidebar_expand">${i18n['expand']}</button>
            </c:if>
        </section>
      </c:if>

      <c:forEach items="${facetResponse.facetGroup}" var="facetGroup">
        <c:if test="${facetGroup.facet.size() != 0}">
          <section class="tgrep sidebar_panel">
            <h3 class="tgrep sidebar_subheading">${i18n[facetGroup.name]}</h3>
            <ul class="tgrep sidebar_list">
              <c:forEach items="${facetGroup.facet}" var="facet">

                <c:choose>
                  <c:when test="${facetGroup.name == 'project.id'}">
                    <c:set var="facetvalue" value="${projectmap[facet.value].name}" />
                    <c:set var="filtermatch" value="${facetGroup.name}:${facet.value}" />
                    <c:set var="filtertitle" value="${facet.value}" />
                  </c:when>
                  <c:when test="${facetGroup.name == 'format' or facetGroup.name == 'edition.language' or facetGroup.name == 'work.genre'}">
                    <!-- properties can not have spaces, so we replace space with underscore, also in language.properties files -->
                    <c:set var="i18nkey" value="${fn:replace(facet.value, ' ', '_')}" />
                    <c:set var="facetvalue" value="${i18n[i18nkey] != null ? i18n[i18nkey] : facet.value}" />
                    <c:set var="filtermatch" value="${facetGroup.name}:${facet.value}" />
                    <c:set var="filtertitle" value="${facet.value}" />
                  </c:when>
                  <c:otherwise>
                    <!-- check if this is a project defined facet and has a 'pdf_'-prefixed i18n label -->
                    <c:set var="i18nkey" value="pdf_${facet.value}" />
                    <c:set var="facetvalue" value="${i18n[i18nkey] != null ? i18n[i18nkey] : facet.value}" />
                    <c:set var="filtermatch" value="${facetGroup.name}:${facetvalue}" />
                    <c:set var="filtertitle" value="${facet.value}" />
                  </c:otherwise>
                </c:choose>

                <li class="tgrep sidebar_item">
                  <c:choose>
                    <c:when test="${not empty filter and filter.contains(filtermatch)}">
                      ${facetvalue}
                    </c:when>
                    <c:otherwise>
                      <a href="?query=${query}&order=${order}&limit=${limit}&mode=${mode}&filter=${facetGroup.name}:${utils:urlencode(facet.value)}${filterQueryString}"
                      class="tgrep sidebar_link" title="${filtertitle}">${facetvalue}</a>
                    </c:otherwise>
                  </c:choose>
                  <span class="tgrep sidebar_count">${facet.count}</span>
                </li>
              </c:forEach>
            </ul>
            <c:if test="${facetGroup.facet.size() > 6}">
              <button class="tgrep sidebar_expand">${i18n['expand']}</button>
            </c:if>
          </section>
        </c:if>
      </c:forEach>
    </aside>

    <main class="tgrep main">
      <c:if test="${results.size() > 0}">
        <%@ include file="components/searchResults.jsp" %>
      </c:if>
      <c:if test="${results.size() eq 0}">
        No Result found within active filters, remove filters to get matches for "${query}"
      </c:if>
    </main>
  </c:if>
</div>

<%@ include file="base/foot.jsp" %>
