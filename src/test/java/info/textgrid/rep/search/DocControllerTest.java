package info.textgrid.rep.search;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import java.util.Locale;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class DocControllerTest {

  @Autowired
  private MockMvc mvc;

  @Value("${lang.default}")
  private String langDefault;

  @Test
  public void getRoot() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("markdown"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/markdown.jsp"))
          .andExpect(status().isOk());
  }

  @Test
  public void getRootEn() throws Exception {
      mvc.perform(
          MockMvcRequestBuilders.get("/")
            .accept(MediaType.TEXT_HTML)
            .locale(Locale.ENGLISH)
          )
          .andExpect(view().name("markdown"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/markdown.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("language", equalTo("en")))
          .andExpect(model().attribute("content", containsString("The TextGrid Repository")));
  }

  @Test
  public void getRootDe() throws Exception {
      mvc.perform(
          MockMvcRequestBuilders.get("/")
            .accept(MediaType.TEXT_HTML)
            .locale(Locale.GERMAN)
          )
          .andExpect(view().name("markdown"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/markdown.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("language", equalTo("de")))
          .andExpect(model().attribute("content", containsString("Das TextGrid Repository")));
  }


  // unsupported lang should fallback to default
  @Test
  public void getRootZH() throws Exception {
      mvc.perform(
          MockMvcRequestBuilders.get("/")
            .accept(MediaType.TEXT_HTML)
            .locale(Locale.CHINESE)
          )
          .andExpect(view().name("markdown"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/markdown.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("language", equalTo("en")))
          .andExpect(model().attribute("content", containsString("The TextGrid Repository")));
  }

  @Test
  public void getNoDoc() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/docs/nothinghere")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().is(404));
  }

  @Test
  public void getGermanSyntax() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/docs/syntax")
            .accept(MediaType.TEXT_HTML)
            .locale(Locale.GERMAN)
          )
          .andExpect(view().name("markdown"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/markdown.jsp"))
          .andExpect(model().attribute("content", containsString("Suchen im TextGrid Repository")))
          .andExpect(status().isOk());
  }

  @Test
  public void getEnglishSyntax() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/docs/syntax")
            .accept(MediaType.TEXT_HTML)
            .locale(Locale.ENGLISH)
          )
          .andExpect(view().name("markdown"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/markdown.jsp"))
          .andExpect(model().attribute("content", containsString("Searching in the TextGrid Repository")))
          .andExpect(status().isOk());
  }

}
