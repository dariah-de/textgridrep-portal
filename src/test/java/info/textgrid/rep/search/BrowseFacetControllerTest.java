package info.textgrid.rep.search;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class BrowseFacetControllerTest {

  @Autowired
  private MockMvc mvc;

  @Test
  public void browseAgentsAscending() throws Exception {
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
        .get("/facet/edition.agent.value?order=term:asc")
        .accept(MediaType.TEXT_HTML);

    mvc.perform(builder)
      .andExpect(view().name("browsefacet"))
      .andExpect(forwardedUrl("/WEB-INF/jsp/browsefacet.jsp"))
      .andExpect(status().isOk());
  }


}
