package info.textgrid.rep.search;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import java.util.Locale;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

// Tests assume that internet access is available and tgsearch is up and running

//@Disabled
@SpringBootTest
@AutoConfigureMockMvc
public class SearchControllerTest {

  @Autowired
  private MockMvc mvc;

  @Test
  public void getRoot() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(status().isOk());
  }

  @Test
  public void getRootEn() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .accept(MediaType.TEXT_HTML)
          .locale(Locale.ENGLISH))
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("language", equalTo("en")));
  }

  @Test
  public void getRootDe() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .accept(MediaType.TEXT_HTML)
          .locale(Locale.GERMAN))
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("language", equalTo("de")));
  }

  // unsupported lang should fallback to en
  @Test
  public void getRootCZ() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .accept(MediaType.TEXT_HTML)
          .locale(Locale.CHINESE))
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("language", equalTo("en")));
  }

  @Test
  public void getAlice() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .queryParam("query", "alice")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().isOk())
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          // should be 20 results because of default limit
          .andExpect(model().attribute("results", hasSize(20)));
  }

  @Test
  public void getAliceLimit30() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .queryParam("query", "alice")
          .queryParam("limit", "30")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().isOk())
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          // should be 30 results because of limit
          .andExpect(model().attribute("results", hasSize(30)));
  }

  @Test
  public void emptySearchWithFilter() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .queryParam("filter", "format:image/jpeg")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().isOk())
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(model().attribute("realQueryString", equalTo("*")));
  }

  // filter strings containing a comma should not be split
  @Test
  public void filterWithComma() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .queryParam("query", "alice")
          .queryParam("filter", "edition.agent.value:Tucholsky, Kurt")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().isOk())
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(model().attribute("filter", hasSize(1)));
  }

  @Test
  public void twoFilters() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .queryParam("query", "alice")
          .queryParam("filter", "edition.agent.value:Tucholsky, Kurt")
          .queryParam("filter", "format:text/xml")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().isOk())
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(model().attribute("filter", hasSize(2)));
  }

  // #57 - NPE when no facetresponse set (empty result)
  @Test
  public void noResults() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/search")
        .queryParam("query", "(genre:\"roman\") AND (work.dateOfCreation.date:\"18??\") AND format:\"text/xml\"")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(view().name("search"))
        .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"));
  }

  // TODO: test gallery image view: changing filters, view or limit should persist the
  // already set properties and not drop them

  // ELTeC has own facets and project config
  @Test
  public void searchProjectFilterWithConfig() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .queryParam("filter", "project.id:TGPR-99d098e9-b60f-98fd-cda3-6448e07e619d")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().isOk())
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(model().attribute("realQueryString", equalTo("*")));
  }

  // Digitale Bibliothek not (yet...?)
  @Test
  public void searchProjectFilterWithoutConfig() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .queryParam("filter", "project.id:TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().isOk())
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(model().attribute("realQueryString", equalTo("*")));
  }

  @Test
  public void pagingLastPage() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .queryParam("start", "9940")
          .queryParam("limit", "10")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().isOk())
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(model().attribute("pager", hasProperty("isLastPageButHasMoreHits", is(true))))
          ;
  }

  @Test
  public void pagingNonLastPage() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/search")
          .queryParam("start", "9930")
          .queryParam("limit", "10")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().isOk())
          .andExpect(view().name("search"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/search.jsp"))
          .andExpect(model().attribute("pager", hasProperty("isLastPageButHasMoreHits", is(false))))
          ;
  }

}
