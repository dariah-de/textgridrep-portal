package info.textgrid.rep.search;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import jakarta.ws.rs.core.MediaType;

@SpringBootTest
@AutoConfigureMockMvc
public class BrowseProjectControllerTest {

  @Autowired
  private MockMvc mvc;

  @Test
  public void browseProjects() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/projects")
        .accept(MediaType.TEXT_HTML))
        .andExpect(view().name("browseprojects"))
        .andExpect(forwardedUrl("/WEB-INF/jsp/browseprojects.jsp"))
        .andExpect(status().isOk());
  }

  @Test
  public void browseProjectWithId() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/project/TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c")
        .accept(MediaType.TEXT_HTML))
        .andExpect(view().name("browse"))
        .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
        .andExpect(status().isOk());
  }

  @Test
  public void browseProjectWithReadme() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/project/TGPR-99d098e9-b60f-98fd-cda3-6448e07e619d")
        .accept(MediaType.TEXT_HTML))
        .andExpect(view().name("browse"))
        .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
        .andExpect(status().isOk())
        .andExpect(model().attribute("readme", containsString("This is the project site of the European Literary Text Collection (ELTeC) in the TextGrid Repository.")))
        ;
  }

}
