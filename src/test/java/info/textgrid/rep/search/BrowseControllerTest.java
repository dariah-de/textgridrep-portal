package info.textgrid.rep.search;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import java.math.BigInteger;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;



@SpringBootTest
@AutoConfigureMockMvc
public class BrowseControllerTest {

  @Autowired
  private MockMvc mvc;

  @Test
  public void browseTei() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wfwb_0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("isTEI", is(true)))
          .andExpect(model().attribute("teiHtml", containsString("Soldaten sind Mörder.")));
  }

  @Test
  public void browseTeiBaseUri() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wfwb")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("isTEI", is(true)))
          .andExpect(model().attribute("teiHtml", containsString("Soldaten sind Mörder.")));
  }

  @Test
  public void browseTeiCorpus() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wfw9_0?mode=xml")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("tocHtml", containsString("Der bewachte Kriegsschauplatz")))
          .andExpect(model().attribute("teiHtml", containsString("Soldaten sind Mörder.")));
  }

  @Test
  public void browseTeiCorpusBaseUri() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wfw9?mode=xml")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("tocHtml", containsString("Der bewachte Kriegsschauplatz")))
          .andExpect(model().attribute("teiHtml", containsString("Soldaten sind Mörder.")));
  }
  /*
  @Test
  public void assumeVoyantForTei() throws Exception {
     mvc.perform(MockMvcRequestBuilders.get("/browse/wh22_0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("isTEI", is(true)))
          .andExpect(model().attribute("teiHtml", containsString("Krethi und Plethi")));
          .andExpect(model().attribute("tools", hasItem(new ViewMode("Voyant", null, false))));
  }
  */

  @Test
  public void browseTeiWithProjectSpecificXslt() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/476b5.0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("isTEI", is(true)))
          .andExpect(model().attribute("teiHtml", containsString("Малла Насрудин и хан работали вместе...")))
          .andExpect(model().attribute("projectXsltUri", is("textgrid:476bf.0"))); // TODO: this may change anytime in the portalconfig, do we need memento?
          ;
  }

  @Test
  public void browseTeiFragment() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/textgrid:kv2q.0?fragment=tg5.2")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("isTEI", is(true)))
          .andExpect(model().attribute("teiHtml", containsString("O schöner, goldner Nachmittag")))
          .andExpect(model().attribute("teiHtml", not(containsString("Hinunter in den Kaninchenbau."))))
          ;
  }

  @Test
  public void browseAggregation() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wmgv_0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("results", hasSize(45)));
  }

  @Test
  public void browseAggregationBaseUri() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wmgv")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk());
  }

  @Test
  public void browseAggregationImageGallery() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wr7j.0")
          .queryParam("mode", "gallery")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("results", hasSize(62)))
          .andExpect(model().attribute("mode", is("gallery")));
  }

  @Test
  public void browseEdition() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wfw9_0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          // TODO: look for viewmode tei-corpus
          .andExpect(model().attribute("results", hasSize(1)));
  }

  @Test
  public void browseEditionBaseUri() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wfw9")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk());
  }

  @Test
  public void browseWork() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wfwc_0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("results", hasSize(1)));
  }

  @Test
  public void browseWorkBaseUri() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wfwc")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("results", not(IsEmptyCollection.empty())));
  }

  @Test
  public void browseImage() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/wvv0_0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(model().attribute("image", is(true)))
          .andExpect(status().isOk());
  }

  @Test
  public void browseDFGViewerMETS() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/30ws1_0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          // TODO: check for viewmode dfg-viewer
          .andExpect(status().isOk());
  }

  @Disabled
  @Test
  public void browseDFGViewerMETS1000() throws Exception {
    // check for go-away
    for (int i=1; i<=1005; i++) {
      mvc.perform(MockMvcRequestBuilders.get("/browse/30ws1_0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          // TODO: check for viewmode dfg-viewer
          .andExpect(status().isOk());
      System.out.println("iiif: " + i);
    }
  }

  @Test
  public void browseRoot() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/browse/root")
        .accept(MediaType.TEXT_HTML))
        .andExpect(view().name("browse"))
        .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
        .andExpect(model().attribute("browseRootAggregations", is(true)))
        .andExpect(model().attribute("results", hasSize(greaterThan(100))))
        .andExpect(status().isOk());
  }

  @Test
  public void browseAggregationHigherRevisionAvailable() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/3qz48.0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("results", hasSize(greaterThan(1))))
          .andExpect(model().attribute("higherRevisionAvailable", is(true)))
          .andExpect(model().attribute("latestRevision", greaterThan(0)))
          .andExpect(model().attribute("revisions", hasEntry(1, "textgrid:3qz48.1")))
          .andExpect(model().attribute("revisions", hasEntry(0, "textgrid:3qz48.0")));
  }

  @Test
  public void nothingFound() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/browse/nothinghere")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().is(404));
  }

  @Test
  public void browsePlainText() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/3qtkq.0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("textContent", containsString("All references to Luxi have been replaced with Meltho")));
  }

  @Test
  public void browseMarkdown() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/40f9t.0")
          .accept(MediaType.TEXT_HTML))
          .andExpect(view().name("browse"))
          .andExpect(forwardedUrl("/WEB-INF/jsp/browse.jsp"))
          .andExpect(status().isOk())
          .andExpect(model().attribute("htmlContent", containsString("Wirkungsgeschichte von Goethes Werk „Zur Farbenlehre“ in Berlin 1810-1832")));
  }



}
