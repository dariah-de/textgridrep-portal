package info.textgrid.rep.search;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class ShelfControllerTest {

  @Autowired
  private MockMvc mvc;

  @Test
  public void showShelf() throws Exception {

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
        .get("/shelf")
        .accept(MediaType.TEXT_HTML);

    mvc.perform(builder)
        .andExpect(view().name("shelf"))
        .andExpect(forwardedUrl("/WEB-INF/jsp/shelf.jsp"))
        .andExpect(status().isOk());
  }

  @Test
  public void addToShelf() throws Exception {

    final MockHttpSession mockHttpSession = new MockHttpSession();

    // add alice
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
        .post("/service/shelf/add")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(mockHttpSession)
        .param("uri", "textgrid:kv2q.0");

    mvc.perform(builder)
        .andExpect(status().isOk())
        .andExpect(content().string("1"));

    // add another
    builder = MockMvcRequestBuilders
        .post("/service/shelf/add")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(mockHttpSession)
        .param("uri", "textgrid:wfw9.0");

    mvc.perform(builder)
        .andExpect(status().isOk())
        .andExpect(content().string("2"));

    // view html
    builder = MockMvcRequestBuilders
        .get("/shelf")
        .session(mockHttpSession)
        .accept(MediaType.TEXT_HTML);

    mvc.perform(builder)
        .andExpect(view().name("shelf"))
        .andExpect(forwardedUrl("/WEB-INF/jsp/shelf.jsp"))
        .andExpect(status().isOk())
        .andExpect(model().attribute("results", hasSize(2)))
        .andExpect(model().attribute("shelfItemString", is("textgrid:kv2q.0,textgrid:wfw9.0")));
        //.andExpect(model().attribute("results", containsString("Alice im Wunderland")));

    // remove one
    builder = MockMvcRequestBuilders
        .post("/service/shelf/remove")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(mockHttpSession)
        .param("uri", "textgrid:wfw9.0");

    mvc.perform(builder)
        .andExpect(status().isOk())
        .andExpect(content().string("1"));

    // clear shelf
    builder = MockMvcRequestBuilders
        .post("/service/shelf/clear")
        .session(mockHttpSession);

    mvc.perform(builder)
        .andExpect(status().isOk())
        .andExpect(content().string("0"));
  }

}
