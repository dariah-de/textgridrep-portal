package info.textgrid.rep.search;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class ErrorControllerTest {

  @Autowired
  private MockMvc mvc;

  @Test
  public void showError() throws Exception {

    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
        .get("/error");

    mvc.perform(builder)
        .andExpect(view().name("error"))
        .andExpect(forwardedUrl("/WEB-INF/jsp/error.jsp"));
  }

  @Test
  public void browseNotFound() throws Exception {
      mvc.perform(MockMvcRequestBuilders.get("/browse/nothinghere")
          .accept(MediaType.TEXT_HTML))
          .andExpect(status().isNotFound())
      ;
  }

}
