package info.textgrid.rep.shared;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import info.textgrid.clients.SearchClient;
import info.textgrid.namespaces.middleware.tgsearch.Response;

public class UtilsTest {

  private static final String TEXTGRIDHOST = "https://textgridlab.org";

  private static SearchClient searchClient;

  @BeforeAll
  public static void setup() {
    searchClient = new SearchClient(TEXTGRIDHOST + "/1.0/tgsearch-public");
  }

  @Test
  public void testImageUrl() {
    Response res = searchClient.infoQuery().getMetadata("textgrid:wtxq.0");
    String imageUrl = Utils.getImageUrl(TEXTGRIDHOST, res.getResult().get(0));
    Assertions.assertEquals(imageUrl, TEXTGRIDHOST + "/1.0/digilib/rest/IIIF/textgrid:wtxq.0/full/,250/0/native.jpg");
  }

  @Test
  public void testImageUrl2() {
    Response res = searchClient.infoQuery().getMetadata("textgrid:16h4p.1");
    String imageUrl = Utils.getImageUrl(TEXTGRIDHOST, res.getResult().get(0));
    Assertions.assertEquals(imageUrl, TEXTGRIDHOST + "/1.0/digilib/rest/IIIF/textgrid:16h4p.1/full/,250/0/native.jpg");
  }

  @Test
  public void testNoImageUrl() {
    Response res = searchClient.infoQuery().getMetadata("textgrid:kv2q.0");
    String imageUrl = Utils.getImageUrl(TEXTGRIDHOST, res.getResult().get(0));
    Assertions.assertEquals(imageUrl, "/images/no_image.svg");
  }

  @Test
  public void testRootCollectionImageUrl() {
    Response res = searchClient.infoQuery().getMetadata("textgrid:mgq5.0");
    String imageUrl = Utils.getImageUrl(TEXTGRIDHOST, res.getResult().get(0));
    Assertions.assertEquals(imageUrl, TEXTGRIDHOST + "/1.0/digilib/rest/IIIF/textgrid:mgqj.0/full/,250/0/native.jpg");
  }

  @Test
  public void testurlEncode() {
    String enc = Utils.urlencode("Bjørnson, Bjørnstjerne");
    Assertions.assertEquals(enc, "Bj%C3%B8rnson%2C+Bj%C3%B8rnstjerne");
  }

  @Test
  public void testImageForMimeType() {
    Assertions.assertEquals(Utils.getImageForMimetype("text/xml"), Utils.MIME_IMG_XML);
    Assertions.assertEquals(Utils.getImageForMimetype("text/tg.work+xml"), Utils.MIME_IMG_WORK);
    Assertions.assertEquals(Utils.getImageForMimetype("text/tg.aggregation+xml"), Utils.MIME_IMG_AGGREGATION);
    Assertions.assertEquals(Utils.getImageForMimetype("text/tg.collection+tg.aggregation+xml"), Utils.MIME_IMG_COLLECTION);
    Assertions.assertEquals(Utils.getImageForMimetype("text/tg.edition+tg.aggregation+xml"), Utils.MIME_IMG_EDITION);
    Assertions.assertEquals(Utils.getImageForMimetype("text/markdown"), Utils.MIME_IMG_TEXT);
    Assertions.assertEquals(Utils.getImageForMimetype("text/plain"), Utils.MIME_IMG_TEXT);
    Assertions.assertEquals(Utils.getImageForMimetype("unknown/plain"), "");

  }
}
