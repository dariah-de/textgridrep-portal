# Switchboard

Die Integration des [Language Resource Switchboard](https://switchboard.clarin.eu/) in das TextGrid Repository (TextGridRep) ist ein wichtiger Schritt im Zuge des Zusammenwachsens der digitalen Forschungsdateninfrastrukturen innerhalb der Geistes- und Sozialwissenschaften ([CLARIAH](https://www.clariah.de/) und [SSHOC](https://www.sshopencloud.eu/)). 
Die im TextGridRep angebotenen Korpora sind vor allem für die Literaturwissenschaft oder allgemein die Computerphilologie relevant.
Für viele Analysen in diesen Bereichen sind im Rahmen der Korpuserstellung und -vorbereitung Tools aus dem Bereich des Natural Language Processing und der linguistischen Annotation, die das Switchboard anbietet, wichtig.

Wir ermöglichen durch die direkte Integration des Switchboards auf komfortable Weise mit Hilfe verschiedener Tools, Texte hinsichtlich linguistischer Merkmale automatisch annotieren und analysieren zu lassen. Dabei arbeitet das Switchboard mit Web-Applikationen; Downloads und Installationen sind folglich nicht nötig und sie greifen auf externe Server zu, so dass Anwender*innen keine großen Rechenkapazitäten benötigen. Der Schritt, Texte aus dem TextGridRep herunterzuladen und dann wieder ins Switchboard hoch zu laden entfällt hiermit.

## Das Switchboard starten

Um die Analyse zu starten, öffnen Sie einen Text und klicken auf der linken Seite des Bildschirms auf das Switchboard (klicken Sie das blaue 🛈, gelangen Sie zu dieser Doku):

![Link zum Switchboard](images/Switchboard_Bild1_neu.png)

Im Anschluss werden Sie zur Seite des Switchboards weitergeleitet.
<!---Wie Sie sehen, gibt es zwei Möglichkeiten: "Switchboard (TEI)" und "Switchboard (txt)". Wählen Sie bitte entsprechend der Entscheidung, auf Grundlage welchen Dateiformats Sie eine Analyse vornehmen möchten. Das TEI-Format bietet im Üblichen eine größere Bandbreite verschiedener Auswertungsmöglichkeiten, allerdings kann es aktuell noch von wenigen Tools innerhalb des Switchboards verarbeitet werden. Außerdem sind zwar die meisten Texte im TextGridRep TEI-kodiert, jedoch noch nicht alle. Um auf möglichst viele verschiedene Tools zugreifen zu können, wählen Sie bitte das txt-Format.
Haben Sie sich für eine Variante des Switchboards entschieden, wechselt der Bildschirm zur Switchboard-Seite.-->

![Dokument- und Toolübersicht](images/Switchboard_Bild2a.png)

Im oberen Bereich Ihres Bildschirms (Pfeil 1) sehen Sie nun Ihre Eingaberessoure, also den Text, den Sie analysieren möchten. Daneben ist das Dateiformat ("Mediatype") verzeichnet. Daneben sehen Sie die ebenfalls automatisch zugewiesene Sprache ("Language"). In diesem Bereich müssen Sie keine Änderungen vornehmen. Prüfen Sie aber bitte, ob das Switchboard automatisch das korrekte Dateiformat und die korrekte Sprache ausgewählt hat und korrigieren Sie diese ggf. Das Switchboard schlägt automatisch eine Auswahl von Tools vor, die mit dem eingegebenen Dateiformat und der Sprache des Textes arbeiten können.
Sie sehen daher (Pfeile 2) verschiedene Tools und Dienste, die am ausgewählten Text automatische Analysen und Annotationen (z.B. Constituency Parsing, Dependency Parsing u.v.m.) durchführen können.


## Tools im Switchboard anwenden

![Toolbenutzung und -dokumentation](images/Switchboard_Bild2b.png)

Um ein Tool auf den ausgewählten Text anzuwenden, klicken Sie bitte auf den grünen "Start Tool"-Bereich (Pfeil 3). Sie werden auf die Website des jeweiligen Tools geleitet, das bereits Ihren Text geladen und zur Verarbeitung vorbereitet hat. Teilweise startet die Verarbeitung automatisch, teilweise müssen Sie diese noch selbst anstoßen. Wünschen Sie eine Dokumentation der Tools, klicken Sie bitte auf das blaue Symbol neben dem Toolnamen (Pfeil 4).


## Das Switchboard auf eigene Kollektionen anwenden

Bislang kann das Switchboard immer nur einen Text gleichzeitig verarbeiten – der Bedarf kann allerdings laut [Switchboard-FAQ-list](https://switchboard.clarin.eu/help) angemeldet werden. Daher haben Sie leider nicht – wie bei [Voyant](https://textgridrep.org/docs/voyant) – die Möglichkeit, das Switchboard auf Ihre selbsterstellte Kollektion in Ihrem [Regal](https://textgridrep.org/docs/shelf) anzuwenden. 


## Referenzen

- [About the Language Ressource Switchboard](https://switchboard.clarin.eu/about)
- Claus Zinn, [The Language Resource Switchboard](https://www.mitpressjournals.org/doi/full/10.1162/coli_a_00329). Computational Linguistics 44(4), pages 631-639, December 2018.
- Claus Zinn. [A Bridge from EUDAT's B2DROP cloud service to CLARIN's Language Resource Switchboard](https://ep.liu.se/ecp/147/004/ecp17147004.pdf). Selected papers from the CLARIN Annual Conference 2017, Budapest, 18-20 September 2017, Linköping University Electronic Press vol. 147, pages 36-45, 2018.
- Claus Zinn. [The CLARIN Language Resource Switchboard](https://www.clarin.eu/sites/default/files/zinn-CLARIN2016_paper_26.pdf). CLARIN 2016 Annual Conference, Aix-en-Provence, France, 2016.
- Claus Zinn, Marie Hinrichs, Emanuel Dima, Dieter van Uytvanck. [The Switchboard specification](https://office.clarin.eu/v/CE-2015-0684-LR_switchboard_spec.pdf) (Milestone 2.2 of the CLARIN-PLUS project).
- Claus Zinn. [D2.5 LR Switchboard (software)](https://office.clarin.eu/v/CE-2016-0881-CLARINPLUS-D2_5.pdf). Deliverable in the CLARIN-PLUS project.


## Weitere Dokumentationen und Tutorials

Weitere Hilfe finden Sie auf der Switchboard-[Hilfeseite](https://switchboard.clarin.eu/help) sowie in den Dokumentationen der einzelnen Tools.
Eine Übersicht über alle im Switchboard angebotenen Dienste und Tools finden Sie [hier](https://switchboard.clarin.eu/tools).


## Kontakt

Haben Sie weitere Fragen zum Switchboard in TextGrid? Dann nehmen Sie bitte [Kontakt](https://textgrid.de/de/kontakt/) zu uns auf!
Bei Fragen zu den einzelnen im Switchboard angebotenen Tools konsultieren Sie bitte zuerst deren Dokumentation und wenden Sie sich dann gerne an die jeweiligen Entwickler.