# Voyant in TextGrid

Die Grundidee der **Voyant Tools** ist es, web-basierte, vielfältige, explorative und analytische Zugänge zu beliebigen Texten oder Textsammlungen zu ermöglichen.

Der Service **Voyant in TextGrid** bietet die Möglichkeit, Texte direkt aus dem TextGrid Repository (sowohl aus der [Digitalen Bibliothek](https://textgrid.de/digitale-bibliothek) sowie anderen Projekten) heraus einer quantitativen Analyse zu unterziehen. 

## Analyse von einzelnen Texten aus TextGrid

Wenn ein Text aus dem TextGrid Repository angezeigt wird (z.B. [Wilhelm Meisters Wanderjahre oder Die Entsagenden](https://textgridrep.org/browse/-/browse/11h9j_0#) von [Johann Wolfgang von Goethe](https://textgridrep.org/search?filter=edition.agent.value%3aGoethe%2c+Johann+Wolfgang+von)), werden weitere Optionen auf dem rechten Menü angezeigt. Eine dieser Optionen ist es, den Text mit Werkzeugen weiter zu bearbeiten oder zu analysieren. Hier finden Sie unter anderem Voyant:


![Link zu Voyant](images/voyant-link.png)

Wenn die Benutzenden auf dieser Seite auf "Voyant" klicken, wird Voyant automatisch mit dem ausgewählten Text geladen: in unserem Beispiel [Wilhelm Meisters Wanderjahre](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:11h9j.0/data):

![Text in Voyant](images/single-text-voyant.png)


## Analyse von Textsammlungen (Korpora) aus TextGrid

Nicht nur einzelne Texte sondern komplette Korpora (Textsammlungen) können aus TextGrid Repository direkt in Voyant übertragen werden. Im [TextGrid Repository](https://textgridrep.org/repository.html) können mehrere Texte zu einem [Regal](/docs/shelf) hinzugefügt werden, um sich ein Korpus zusammenzustellen, das der eigenen Forschungsfrage angepasst ist. Dieses Regal bietet gleichwohl die Möglichkeit, unter „Werkzeuge“ die ausgewählten Texte z. B. direkt mit den Voyant Tools zu analysieren.

Beispielsweise kann man [12 dramatische Texte von Friedrich Schiller](https://textgridrep.org/search?query=&filter=work.genre%3Adrama&filter=edition.agent.value%3ASchiller%2C+Friedrich&start=10&limit=10&order=relevance) zu einem [Regal hinzufügen](/docs/shelf), und dann [diese Sammlung als gesammeltes Korpus durch Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/aggregator/zip/textgrid:tz6r.0,textgrid:tz9d.0,textgrid:txtj.0,textgrid:tzgk.0,textgrid:tx4z.0,textgrid:tz39.0,textgrid:v0d8.0,textgrid:v0hv.0,textgrid:v0nx.0,textgrid:tz0c.0,textgrid:v0fv.0,textgrid:twt3.0?meta=false&only=text/xml) analysieren lassen:

![Textsammlung in Voyant](images/corpus-voyant.png)

Um über weitere Funktionalitäten der Option „Regal“ zu erfahren, lesen Sie [hier mehr](/docs/shelf).

## Mehrwert der Integration von Voyant in TextGrid

Die Integration ermöglicht die Nutzung von Voyant ohne weitere Vorkenntnisse. Somit müssen sich die Benutzenden mit der Verwaltung der Dokumente nicht befassen. Konkret bedeutet dies: Sie müssen die Texte nicht aus dem TextGrid Repository herunterladen, auf dem eigenen Gerät speichern und danach in Voyant erneut hochladen. Dieser Service wendet sich besonders an Lernende und Lehrende aus der Sprach- und Literaturwissenschaft, die einen Einstieg in die quantitative Analyse innerhalb der Textsammlungen des TextGrid Repository, wie z. B. der [Digitalen Bibliothek](https://textgrid.de/digitale-bibliothek) suchen. Beispielsweise können Lehrende den Link zu Voyant Tools, der via TextGridRep erzeugt wurde und eine bestimmte Textsammlung enthält, direkt an die Studierenden schicken.

## Weitere Funktionalitäten von Voyant
Voyant in TextGrid zeigt für ausgewählte Texte Worthäufigkeiten, Verteilungen und Kookkurrenzen und erlaubt es, auf einfache Art Eigenschaften der ausgewählten Texte zu explorieren und zu visualisieren. So lassen sich stilistische Besonderheiten, wie die Verwendung von Wörtern in gegensätzlichen Zusammenhängen, Wortballungen an charakteristischen Stellen im Gesamtkorpus oder im Einzeltext, auf einen Blick erkennen und darstellen. Dies kann im Rahmen literaturwissenschaftlicher Analysen zur Unterstützung der Interpretation aus dem Close-Reading sein oder erst den Anstoß geben, einen oder viele Texte aus einem anderen quantitativen Blickwinkel zu betrachten.

## Weitere Beispiele

- [*Wilhelm Meisters Wanderjahre oder Die Entsagenden*](https://textgridrep.org/browse/-/browse/11h9j_0#) von [Johann Wolfgang Goethe](https://textgridrep.org/search?filter=edition.agent.value%3aGoethe%2c+Johann+Wolfgang+von), in  [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:11h9j.0/data).
- [*Der Sandmann*](https://textgridrep.org/browse/q5sf_0) von [E. T. A. Hoffmann](https://textgridrep.org/search?filter=edition.agent.value%3aHoffmann%2c+E.+T.+A.), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:q5sf.0/data)
- [*Reise um die Erde in 80 Tagen*](https://textgridrep.org/browse/wr7p_0#tg512.2.1) von [Jules Verne](https://textgridrep.org/search?filter=edition.agent.value%3aVerne%2c+Jules) (auf Deutsch), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:wr7p.0/data)
- [*Macbeth*](https://textgridrep.org/browse/vn2s_0) von [William Shakespeare](https://textgridrep.org/search?filter=edition.agent.value%3aShakespeare%2c+William) (auf Deutsch), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:vn2s.0/data)
- [*Don Quijote*](https://textgridrep.org/browse/kv63_0) von [Miguel de Cervantes](https://textgridrep.org/search?filter=edition.agent.value%3aCervantes+Saavedra%2c+Miguel+de) (auf Deutsch), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:kv63.0/data)
- [*Die Göttliche Komödie*](https://textgridrep.org/browse/m5s9_0) von [Dante Alighieri](https://textgridrep.org/search?filter=edition.agent.value%3aDante+Alighieri) (auf Deutsch), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:m5s9.0/data)
- [*La Celestina (Tragicomedia de Calisto y Melibea)*](https://textgridrep.org/browse/3rxbp_0) von [Fernando de Rojas](https://textgridrep.org/search?filter=edition.agent.value%3aFernando+de+Rojas) (auf Spanisch), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:3rxb4.0/data).

Für die Auswahl von Texten bietet sich ein Blick in die Inhalte des TextGridRep an, die [nach Autor*in](/facet/edition.agent.value?order=term:asc), [nach Genre](/facet/work.genre), [nach Dateityp](/facet/format) und [nach Projekt](https://textgridrep.org/projects) gefiltert werden können, an.

## Referenzen

- [Sinclair, Stéfan and Geoffrey Rockwell, 2016. Voyant Tools. Web. http://voyant-tools.org/](http://voyant-tools.org/).

## Weitere Dokumentation und Tutorials

- [Dokumentation von Voyant Tools](https://voyant-tools.org/docs/#!/guide/start).
- [Tutorial von den Entwickler von Voyant geschrieben](https://voyant-tools.org/docs/#!/guide/tutorial) (auf Englisch).
- [Einführender Video über Voyant von forTEXT & CATMA](https://www.youtube.com/watch?v=KbBHTlXoZO4) (auf Deutsch).
- [Gutiérrez de la Torre, Silvia. “Análisis de corpus con Voyant Tools.” The Programming Historian en español, no. 3.](https://doi.org/10.46430/phes0043) (auf Spanisch).


## Beschreibung der Integration des Werkzeugs in TextGrid

Es handelt sich dabei um eine Instanz der [Voyant Tools](https://voyant-tools.org/), die direkt über die Webseite des TextGridRep heraus aufgerufen werden kann.

## Weitere Fragen?

Wenn Sie weitere Fragen zur Integration von Voyant und TextGrid haben, nehmen Sie [Kontakt](https://textgrid.de/de/kontakt/) mit uns auf.
