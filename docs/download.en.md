# Download

You can download the objects contained in TextGridRep in various formats. The TextGrid user manual explains in detail what [types of objects](https://wiki.de.dariah.eu/display/TextGrid/TextGrid-Objekte) there are.


## Differentiation of objects in TextGridRep

As an example, this is the catalog view when searching "Ideen herder".

![ListeNachSuche - Download](images/liste-download.png)

The different object types such as **texts, editions or works** are identified by symbols next to the title of the object. Here is an example of the **edition** type:

![Icon1 - Download](images/icon1ok-download.png)

And here for the **text** type:

![Icon2 - Download](images/icon2ok-download.png)

For editions and aggregations, you can only access the metadata via "Download" in the catalog view, but not the object Plant and the associated full text.


## Download texts

**Users are often interested in downloading individual texts or text collections.** For this purpose, it is useful to filter the search results by file type "txt / xml" or "text / tg.work + xml" (selectable in the menu on the left).
In the screenshot you can see the filter selection for the example of Goethe's "Faust, Part One": file type "text / tg.work + xml". To go straight to the text, click on the heading.

![Dateityp/Textauswahl - Download](images/Screenshot2-download.png)

The textual resources are available as XML-TEI, plain text (txt), e-book (epub) or in HTML format. You can also download a zip file that contains XML files with the metadata and the main resource (in TEI-XML). In the next screenshot you can see the different formats in the menu on the left. You can access this view at the object level as described above via the title selection in the catalog view (click on the title heading).

![Dateityp/Listeformate - Download](images/listeformate-download.png)
