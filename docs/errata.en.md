# Errata

Currently, errata only refers to works from the [digital library in TextGrid](https://textgrid.de/en/digitale-bibliothek).
Since the bibliographic metadata of the digital library have been extracted from the template, errors cannot be ruled out any more than in a printed work. 
Once published in the TextGrid Repository, these known 'printing errors' can only be improved in the event of a revision (similar to a new edition) of the entire work (the digital library).
Until this revision, known errata are listed here. The same applies to all other texts in the TextGrid Repository, where revisions are easier due to smaller corpora.
If you find similar errata yourself, please report them to: [info@textgrid.de](info@textgrid.de)

1. [Lazarillo de Tormes](https://textgridrep.org/browse/sc97_0?lang=en): Hurtado de Mendoza was incorrectly entered as the author:
TextGrid Repository (2012). Mendoza, Diego Hurtado. Lazarillo de Tormes. Digital library. Right: Anonymous. Lazarillo de Tormes.
This is already correctly stated in the notes on the work: “This is an anonymous work by various authors. Hurtado de Mendoza is also attributed. Created before 1530.
First printed: probably Burgos 1552/53 (text lost). The earliest three prints preserved are: Burgos 1554, Alcalá 1554 and Amberes 1554. 
All three are based on lost editions and not on manuscripts. Here after translation from I.G. Keil."

2. In the case of the brothers Paul and Josef Weidmann, participation in authorship is often not clarified, the metadata from different sources contradict each other:
Example of the drama [Der Dorfbarbier](https://textgridrep.org/browse/x3zw_0?lang=en): Paul Weidmann is listed as the author, but the name of the brother is written in the notes: "Composed by Johann Baptist Schenk.
Libretto in collaboration with Josef Weidmann. First performed on October 30, 1979, Kärntnertor-Theater, Vienna."
The [German Digital Library](https://www.archivportal-d.de/item/AA6SRIM6C2DEQS6XRL6YSIYB3PJCSD7A?lang=en) also does not resolve this contradiction. The page on Paul Weidmann in the [German Bibliography](https://www.deutsche-biographie.de/sfz84785.html?language=en) offers a detailed explanation.

3. [Lebensläufe nach Aufsteigender Linie nebst Beilagen A, B, C](https://textgridrep.org/browse/q5q0.0) from Theodor Gottlieb von Hippel:
    - In /Zweiter Theil/ the first 86 pages are missing.
    - In /Dritter Theil. Zweiter Band/ the first 163 pages are missing.
