# Errata

Aktuell beziehen sich die Errata nur auf Werke aus der [Digitalen Bibliothek bei TextGrid](https://textgrid.de/digitale-bibliothek).
Da die bibliografischen Metadaten der [Digitalen Bibliothek](https://textgrid.de/digitale-bibliothek) aus der Vorlage extrahiert wurden, sind Fehler ebensowenig auszuschließen wie in einem gedruckten Werk. Einmal im TextGrid Repository veröffentlicht, können diese bekannten ‘Druckfehler’ nur bei einer Revision (ähnlich einer Neuauflage) des Gesamtwerks (der Digitalen Bibliothek) verbessert werden. Bis zu dieser Revision werden bekannte Errata hier aufgeführt. Gleiches gilt für alle anderen Werke im TextGrid Repository ebenfalls - dort sind Revisionen aufgrund der kleineren Korpora allerdings leichter.
Sollten Sie selbst ähnliche Errata auffinden, melden Sie diese bitte an: [info@textgrid.de](info@textgrid.de)

1. [Lazarillo de Tormes](https://hdl.handle.net/11858/00-1734-0000-0004-338C-9): Hier wurde als Autor fälschlicherweise Hurtado de Mendoza eingetragen:
TextGrid Repository (2012). Mendoza, Diego Hurtado. Lazarillo de Tormes. Digitale Bibliothek.  
   Richtig: *Anonym*. Lazarillo de Tormes.
   In den Notizen zum Werk ist dies bereits richtig vermerkt: “Es handelt sich hier um ein anonymes Werk verschiedener Autoren, das u.a. auch Hurtado de Mendoza zugeschrieben wird. Entstanden vor 1530. Erstdruck: vermutlich Burgos 1552/53 (Text verloren). Die frühesten drei erhaltenen Drucke sind: Burgos 1554, Alcalá 1554 und Amberes 1554. Alle drei basieren auf verlorenen Editionen und nicht auf Manuskripten. Hier nach Übers. v. I.G. Keil.”

2. Im Fall der Brüder Paul und Josef Weidmann ist die Beteiligung an der Autorschaft oft nicht eindeutig geklärt, die Metadaten verschiedener Quellen widersprechen sich:
  Beispiel das Drama [Der Dorfbarbier](https://hdl.handle.net/11858/00-1734-0000-0005-97BA-0): Hier wird Paul Weidmann als Autor geführt, aber in den Notizen steht der Name des Bruders: "Komponiert von Johann Baptist Schenk. Libretto in Zusammenarbeit mit Josef Weidmann. Uraufführung am 30.10.1796, Kärntnertor-Theater, Wien."  
    Auch die [Deutsche Digitale Bibliothek](https://www.archivportal-d.de/item/AA6SRIM6C2DEQS6XRL6YSIYB3PJCSD7A?query=affiliate_fct_role_normdata%3A%28%22http%3A%2F%2Fd-nb.info%2Fgnd%2F119497921_1_affiliate_fct_involved%22%29&isThumbnailFiltered=false&rows=20&offset=0&viewType=list&hitNumber=4) löst diesen Widerspruch nicht auf. Genaue Erläuterung bietet die Seite zu Paul Weidmann bei der [Deutschen Bibliographie](https://www.deutsche-biographie.de/sfz84785.html).

3. [Lebensläufe nach Aufsteigender Linie nebst Beilagen A, B, C](https://textgridrep.org/browse/q5q0.0) von Theodor Gottlieb von Hippel:
    - In /Zweiter Theil/ fehlen die ersten 86 Seiten.
    - In /Dritter Theil. Zweiter Band/ fehlen die ersten 163 Seiten.
