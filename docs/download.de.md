# Herunterladen

Sie können die in TextGridRep enthaltenen Objekte in verschiedenen Formaten herunterladen. Welche [Arten von Objekten](https://wiki.de.dariah.eu/display/TextGrid/TextGrid-Objekte) es gibt, erklärt ausführlich das TextGrid Nutzerhandbuch.

## Unterscheidung von Objekten in TextGridRep

Nach einer Suchanfrage zu "Herder Ideen" haben Sie beispielsweise folgende Katalogansicht:

![ListeNachSuche - Download](images/liste-download.png)

Die verschiedenen Objekttypen wie **Texte, Editionen, oder Werke** sind durch Symbole neben dem Titel des Objekts gekennzeichnet.
Hier ist ein Beispiel für den Typ **Edition**:

![Icon1 - Download](images/icon1ok-download.png)

Und hier für den Typ **Text**:

![Icon2 - Download](images/icon2ok-download.png)

Bei Editionen und Aggegrationen können Sie in der Katalogansicht lediglich die Metadaten über "Herunterladen" erreichen, nicht aber das Objekt Werk und den dazugehörigen Volltext.

## Texte herunterladen

**Benutzer*innen sind häufig daran interessiert, einzelne Texte oder Textsammlungen** herunterzuladen. Dazu ist es sinnvoll, die Suchergebnisse nach Dateityp "txt/xml" oder "text/tg.work+xml" (auf der linken Seite im Menu auswählbar) zu filtern.
Im Screenshot sehen Sie am Beispiel von Goethes "Faust. Der Tragödie erster Teil" die Auswahl des Filters: Dateityp "text/tg.work+xml". Um danach direkt zum Text zu gelangen, klicken Sie  auf die Überschrift.

![Dateityp/Textauswahl - Download](images/Screenshot2-download.png)

Die textuellen Ressourcen sind als XML-TEI, Plain Text (txt), E-Book (epub) oder im HTML-Format verfügbar. Sie können auch eine Zip-Datei herunterladen, die XML-Dateien mit den Metadaten und der Hauptressource (in TEI-XML) enthält. Im nächsten Screenshot sehen Sie die verschiedenen Formate im Menü links. Zu dieser Ansicht auf Objektebene gelangen Sie wie oben beschrieben über die Titelauswahl in der Katalogansicht (Klicken auf die Titelüberschrift).

![Dateityp/Listeformate - Download](images/listeformate-download.png)