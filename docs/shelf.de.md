# Regal

Das TextGrid Repository bietet über die Funktion „Regal“ die Möglichkeit, individuelle Korpora in der Form von Kollektionen zusammenzustellen. Diese Korpora können dann heruntergeladen oder an weitere Tools geschickt werden.

## Texte zum Regal hinzufügen

Die Benutzenden können zum Beispiel ein Korpus von Texten aus dem Bereich der Schauerliteratur des 19. Jahrhunderts zusammenstellen wollen. Daher würden sie jeweils die gewünschten Texte auswählen, zum Beispiel von E.A. Poe, E.T.A. Hoffmann, Jules Verne, Lena Christ und Charles Baudelaire. Jeder Text wird somit zum persönlichen Regal hinzugefügt. 

![select - shelf](images/Select-shelf.png)

## Texte im Regal verwalten

Das Regal kann danach über die Ansicht "Regal" angesehen und weiter verwaltet werden. Es können wieder einzelne Objekte entfernt werden, einzeln betrachtet und analysiert (annotiert) werden oder einzeln oder gesammelt heruntergeladen und analysiert werden.


## Regal herunterladen

Die Texte aus dem Regal können in verschiedenen Formaten heruntergeladen werden:

- E-Book: Alle Texte in einem ePUB Dokument für die Lektüre
- ZIP:  Alle Texte in einem Zip-Ordner, mit unterschiedlichen Ordnern für jeden Text, mit jeweils separaten Metadaten und XML-TEI Dateien.
- TEI-Corpus: Alle Texte in einem einzigen XML-TEI Dokument mit allen Metadaten.

![Download - shelf](images/Download-shelf.PNG)


## Textkollektion aus dem Regal analysieren

Die vollständige Textkollektion kann außerdem mit dem Werkzeug [Voyant](/docs/voyant) direkt weiter analysiert werden.

