# Shelf

The "Shelf" function in the TextGrid Repository offers the option of compiling individual corpora in the form of collections. This corpora can then be downloaded or used with other tools.


## Add texts to the shelf

For example, users may want to compile a corpus of texts from the field of show literature in the 19th century. In this case they would select the desired texts, for example by E.A. Poe, E.T.A. Hoffmann, Jules Verne, Lena Christ and Charles Baudelaire. Each text is thus added to the personal shelf.

![select - shelf](images/Select-shelf.png)


## Manage texts on the shelf

The shelf can then be viewed and further managed via the "Shelf" view. Individual objects can be removed, viewed and analyzed individually (annotated), or downloaded and analyzed individually or collectively.


## Download shelf

The texts from the shelf can be downloaded in various formats:

 - E-book: All texts in one ePUB document.
 - ZIP: All texts in a zip folder, with different folders for each text, each with separate metadata and XML-TEI files.
 - TEI-Corpus: All texts in a single XML-TEI document with all metadata.

![Download - shelf](images/Download-shelf.PNG)


## Analyze text collection from the shelf

The complete text collection can also be directly analyzed further using the [Voyant](/docs/voyant) tool.