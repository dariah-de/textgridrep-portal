# Annotation Viewer
<!---Alternativ könnte es auch "Annotate", "Annotate in TextGrid" oder "Annotation Sandbox" (die aber superschlecht funktioniert) heißen.---> 

Der Annotation Viewer ist ein webbasiertes Annotationstool, das es den Nutzenden ermöglicht, Texte mit einfachen Annotationen auszuzeichnen.

Die Beta-Version der **Annotate-Funktion im TextGridRepository** (TextGridRep) bietet die Möglichkeit, Texte aus dem TextGridRep heraus in den Annotation Viewer zu laden, um dort direkt die Annotation durchzuführen.
Der Annotation Viewer ist eine Webapplikation; Downloads und Installationen sind folglich nicht nötig und die Nutzenden greifen auf externe Server zu, so dass Anwendende keine großen Rechenkapazitäten benötigen.
Der Schritt, Texte aus dem TextGridRep herunterzuladen und dann wieder in den Annotation Viewer zu laden entfällt hiermit.

## Den Annotation Viewer starten

Um die Analyse zu starten, öffnen Sie einen Text und klicken Sie auf der linken Seite des Bildschirms auf _Annotate_ (klicken Sie das blaue 🛈, gelangen Sie zu dieser Doku):

![Link zu Annotate](images/Annotate_1_Textanzeige.png)

Sollten Sie noch nicht in der DARIAH-AAI eingeloggt sein, werden Sie zuerst zu folgender Login-Seite geleitet, auf der Sie sich entweder direkt mit Ihrem DARIAH-Login einloggen können oder sich durch Ihre Institution identifizieren, die an der DARIAH-AAI teilnimmt.
Sollten Sie bereits eingeloggt sein, entfällt dieser Schritt.
Der Login garantiert, dass Ihre Annotationen gespeichert werden und Gruppenannotationen möglich sind.

![Einwahl der Organisation](images/Annotate_2_Einwahl_Organisation.png)

Im Anschluss werden Sie zur Seite des Annotation Viewers weitergeleitet.

![Startansicht Annotate](images/Annotate_3_Startseite_Annotate.png)

In der Mitte (Pfeil 1) sehen Sie nun die Metadaten des Textes, den Sie geladen haben, entsprechend der Eintragung im TextGridRep.
Darunter (Pfeil 2) befindet sich der Text selbst.
Im oberen Bereich (Pfeil 3) sehen Sie in der grauen Leiste das Menü; dieses wird unten genauer erklärt.
Schließlich gibt es noch (Pfeil 4) einen "About"-Hinweis zur Art und Weise, wie die Annotation technisch durchgeführt wird.
Durch Klick auf das _x_ rechts in diesem Fenster können Sie den Hinweis schließen.

## Annotationen im Annotation Viewer erstellen

Der Annotation Viewer ermöglicht freies Annotieren einer Textpassage oder eines Wortes an einer beliebigen Stelle des Textes, um dort eine Annotation zu hinterlegen.
Markieren Sie dazu die gewünschte Passage im Text.

![Text markieren](images/Annotate_4_Text_markieren.png)

Sie sehen nun einerseits in der grauen Fußzeile den XPath der Stelle angezeigt, auf der Sie sich gerade mit Ihrer Maus befinden.
Andererseits sehen Sie nun direkt rechts neben Ihrem markierten Text eine Sprechblase mit einem Schreiben-Symbol.
Um die Annotation durchzuführen, klicken Sie bitte auf die Sprechblase.
Die Sprechblase vergrößert sich nun und der Eingabebereich erscheint:

![Annotation durchführen](images/Annotate_5_Annotation_durchfuehren.png)

Im oberen Bereich unter "Comments" können Sie Ihren Kommentar zu der Textpassage hinterlegen.
Im unteren Bereich können Sie Tags vergeben, die als Kategorien fungieren, mit denen Sie Ihre Annotationen gruppieren können.
Möchten Sie mehrere Tags vergeben, trennen Sie diese mit einem einfachen Leerzeichen.
Um die Annotation zu speichern, klicken Sie auf "Save".
Sie ist damit auch gespeichert, wenn Sie die betreffende Seite mit Ihrem Account wieder aufrufen.
Fahren Sie nun mit Ihrem Cursor über die markierte Textpassage, wird Ihnen Ihre Annotation angezeigt.
Dort befindet sich auch in der rechten oberen Ecke die Möglichkeit, die Annotation zu bearbeiten.
Textpassagen können auch überlappend (oder deckungsgleich) annotiert werden.
Markieren Sie in diesem Fall wie oben die gesamte Textpassage – unabhängig davon, ob sich in ihr bereits Annotationen befinden.
Fahren Sie nun mit der Maus über eine Passage, zu der mehrere Annotationen hinterlegt sind, werden Ihnen diese alle in der Reihenfolge der Eingabe angezeigt.
Annotationen sind übrigens auch in den Metadaten, Fußnoten oder Randbemerkungen möglich.

## Annotationen ansehen und exportieren

Um sich eine Liste der getätigten Annotationen anzusehen, klicken Sie im linken oberen Bereich auf "Annotation Manager".

![Tabelle der Annotationen](images/Annotate_6_Annotationstabelle.PNG)

Hier sehen Sie alle Annotationen, die Sie getätigt haben – auch von anderen Texten.
In der Tabelle steht die URL der Webseite, auf der die Annotationen durchgeführt wurden, gefolgt von Ihren Kommentaren und Tags.
"Quotes" entspricht dem Bereich, den Sie im Text für Ihre Annotation markiert haben.
Schließlich folgt der Zeitstempel von Erstellung und Modifikation der Annotation und die Gruppenzugehörigkeit.
Sämtliche Spalten sind alphabetisch sortierbar.
Im obigen Menü können Sie u.a. eine CSV-Tabelle all Ihrer Annotationen extrahieren oder die Anzahl der sichtbaren Spalten verändern.
Haben Sie eine oder mehrere Annotationen ausgewählt, können Sie diese zu Gruppen hinzufügen oder in Ihren eigenen Bereich im [TextGrid Laboratory](https://textgrid.de/download) exportieren.

## Gruppenannotationen

Das Gruppenfeature wurde eingeführt, um ein besseres Annotationsmanagement betreiben zu können, Annotationen zu koordinieren und miteinander zu teilen.
Um eine Gruppe zu erstellen, klicken Sie im oberen Bereich auf "Groups".
Im folgenden Fenster sehen Sie eine Übersicht über Ihre bisherigen Gruppenmitgliedschaften und welche Gruppen Sie als Admin führen:

![Gruppenübersicht](images/Annotate_7_Groups.PNG)

Klicken Sie hier auf "create a group", um eine neue Gruppe anzulegen:

![Gruppe erstellen](images/Annotate_8_Groupe_creation.PNG)

Hier können Sie in der ersten Zeile einen Gruppennamen festlegen; in der zweiten Zeile können Sie weitere DARIAH-Nutzende mittels deren Mailadresse zu Ihrer Gruppe hinzufügen.
Klicken Sie auf "Create", so ist Ihre Gruppe erstellt und wird Ihnen in der Liste Ihrer als Admin geführten Gruppen (wie im vorherigen Bild die Gruppe "Test Documentation") angezeigt.

Wenn Sie nun in Ihrem Annotation Manager Ihre Annotationen einer Gruppe zuweisen, so werden diese auch für alle anderen Gruppenangehörige sichtbar.
Dieses Feature ermöglicht folglich kollaborative Annotationsarbeit.
Dabei bietet es sich an, für schnelle Findbarkeit Ihre Texte im TextGridRep in Ihrem [Regal](https://textgridrep.org/docs/shelf) zu speichern.

## Kontakt

Haben Sie weitere Fragen zur Annotate-Funktion im TextGridRep?
Dann nehmen Sie gerne [Kontakt](https://textgrid.de/de/kontakt/) zu uns auf!
