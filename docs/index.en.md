# The TextGrid Repository

The TextGrid Repository is a long-term archive for humanities research data. It provides an extensive, searchable, and reusable repository of texts and images. Aligned with the principles of Open Access and FAIR, the TextGrid Repository was awarded the [CoreTrustSeal](https://www.coretrustseal.org/why-certification/certified-repositories/) in 2020. For researchers, the TextGrid Repository offers a sustainable, durable, and secure way to publish their research data in a citable manner and to describe it in an understandable way through required metadata. Read more about sustainability, FAIR and Open Access in the [Mission Statement](/docs/mission-statement) of the TextGrid Repository.

The vast majority of the texts are XML/TEI encoded in addition to the plain text format, allowing for diverse reuse. The repository was established with the acquisition of the [Digital Library](https://textgrid.de/en/digitale-bibliothek) and is continually evolving based on the [TextGrid Community](https://textgrid.de/en/community). Through numerous edition projects, which are created in the virtual research environment of the [TextGrid Laboratory](https://textgrid.de/en/download), manuscripts (images) as well as transcriptions (XML/TEI encoded text data) are available (such as the the [Library of Neology](/search?filter=project.value%3aNeologie) or the project on German-French travel correspondence [ARCHITRAVE](/search?filter=project.value%3aArchitrave)).

Accordingly, the content is partly project-specific and is growing over time. The basis is an extensive corpus of world literature from the beginning of the history of printed books to the 20th century, consisting of texts by around 600 authors: the TextGrid [Digital Library](https://textgrid.de/en/digitale-bibliothek) contains world literature written or translated in German. Nevertheless, the TextGrid Repository has no language restriction and foreign language texts are published here depending on the project context.

In addition to the advanced [search](/docs/syntax), the content of the TextGrid Repository is also explorable by filtering [by author](/facet/edition.agent.value?order=term:asc), [by genre](/facet/work.genre), [by file type](/facet/format), and [by project](https://textgridrep.org/projects).

All published content is open-access, and should be referenced as usual according to the citation suggestion provided in each case. The TextGrid Repository offers the possibility to compile individual collections via the [shelf function](/shelf). These can be downloaded collectively in XML or TXT formats or examined directly with a range of [digital tools](/docs/switchboard).

**Participation**

Would you like your own XML encoded files to be archived, made quotable and accessible through the TextGrid Repository? Then contact us: <https://textgrid.de/en/kontakt/>
