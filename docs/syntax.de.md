# Suchen im TextGrid Repository

## Die Syntax von Apache Lucene

Das TextGrid Repository nutzt die Syntax von Apache Lucene 2.9.4. Die Mechanismen dieser Syntax werden im Folgenden erläutert. Die Darstellung folgt dabei weitgehend der Zusammenfassung auf der [Website von Apache Lucene](https://lucene.apache.org/core/5_1_0/queryparser/org/apache/lucene/queryparser/classic/package-summary.html#package_description).

Suchanfragen werden in dieser Syntax mit Suchbegriffen und sogenannten Operatoren gebildet. Suchbegriffe können über Felder näher bestimmt werden.


## Suchbegriffe und -felder

Es können ebenso einzelne Worte als Suchbegriffe dienen wie Phrasen, die über doppelte Anführungszeichen als zusammengehörig gekennzeichnet werden, z.B. "TextGrid Repository". Suchbegriffe können auch nur auf bestimmte Felder bezogen sein:

    Feldname:Suchbegriff

oder

    Feldname:"Mehrteilige Phrase"

TextGrid kennt die verschiedene Felder wie

* „title“ für den Werktitel
* „edition.agent.value“ für den Verfasser
* „language“ für die Sprache des Textes
* „notes" für Anmerkungen zum Text
* „genre“ für Gattungen
* „rightsHolder“ für den Inhaber der Rechte an der digitalen Textfassung
* „work.dateOfCreation.date“ sowie „work.dateOfCreation.notBefore“ und „work.dateOfCreation.notAfter“ für Datierungen der Werke.

Die „Advanced Search“ bietet die Möglichkeit, diese Felder für die Suche in den Metadaten direkt auszuwählen und auch mit Operatoren zu Suchanfragen zu verbinden.

Suchbegriffe können außerdem in verschiedener Weise abgewandelt werden. Es gibt verschiedene Platzhalter, Möglichkeiten für eine ungenaue Suche, Angabe von Distanzen und Reihen sowie die Berücksichtigung der Relevanz.


* **Platzhalter:** Bei einzelnen Worten kann ? für einzelne beliebige Zeichen, * für beliebig viele Zeichen verwendet werden, z.B. ? und * am Anfang des Wortes können die Suche verlängern.
* **Ungenaue Suche:** Mit angehängtem ~ kann ein Wort mit einer gewissen Ungenauigkeit gemäß der Levenshteindistanz gefunden. Auf das ~ kann ein Zahlenwert zwischen 0 und 1 folgen. Je näher der Wert der 1 ist, umso größer ist die geforderte Ähnlichkeit. Der Standardwert beträgt 0.5.
* **Abstände:** Bei Phrasen kann mit einem angehängten ~ und einem folgenden Zahlenwert der Abstand zwischen den einzelnen Worten bestimmte werden, z.B. `"TextGrid Repository"~10`. Der Zahlenwert bestimmt die Anzahl der Worte, die zwischen den gesuchten Worten liegen dürfen. Die „Advanced Search“ erlaubt, diesen Wert auch direkt über die Eingabemaske festzulegen.
* **Reihen:** Verknüpft man Suchwerte mit "TO" werden alle Werte innerhalb eines Felder zwischen ihnen gefunden. Dies gilt ebenso für Zahlenwerte wie für Worte. Bei Worten wird die alphabetische Ordnung. [] dienen für Suchen inklusive der genannten Werte, {} für Suchen, die diese nicht berücksichtigen. Z.B. findet `edition.agent.value:[Aristophanes TO Zuckmayer]` alle Namen von Autor*innen zwischen „Aristophanes“ und „Zuckmayer“ inklusive dieser beiden Namen.
* **Relevanz:** Mit angehängtem ^ und folgendem Zahlenwert können Suchbegriffe oder Phrasen als besonders relevant gekennzeichnet werden, z.B. `TextGrid^5 Repository`. Der Standardwert ist 1.

Einige Zeichen müssen mit \ maskiert werden: `+ - && || ! ( ) { } [ ] ^ " ~ * ? : \`.


## Operatoren

Lucene verwendet Boolesche Operatoren, um Suchbegriffe- und phrasen zu verknüpfen. Der Standardwert ist OR bzw. ||. Boolesche Operatoren müssen, wenn sie als Wort ausgeschrieben werden, in Großbuchstaben stehen.

* **AND (auch &&):** Es werden Texte gefunden, die alle gesuchten Begriffe enthalten.
* **+:** Dies kennzeichnet, dass der folgende Suchbegriff vorhanden sein muss.
* **NOT (auch ! oder -):** Dies kennzeichnet, dass der folgende Suchbegriff nicht vorhanden sein darf.  Die Verwendung zu Beginn einer Suchabfrage kann den Suchvorgang verlangsamen.

Lucene unterstützt Klammerung zur Verknüpfung Boolescher Operatoren, z.B. findet `TextGrid AND (Laboratory OR Repository)` alle Texte, die das Wort „TextGrid“ enthalten sowie das Wort „Laboratory“ oder „Repository“. Dieser Mechanismus kann auch in Bezug auf Felder verwendet werden.
