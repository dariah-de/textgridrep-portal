# Voyant in TextGrid

**Voyant Tools** aims to provide web-based access to collections of texts for exploration and analysis.

The service **Voyant in Textgrid** allows users to do quantitative analyses of the [Digital Library](https://textgrid.de/en/digitale-bibliothek) and other projects, directly in the TextGrid Repository. 


## Analyzing individual texts in TextGrid

When displaying a text in the TextGrid Repository, the menu on the left lists Voyant under the tools category.

![Link zu Voyant](images/voyant-link.png)

Clicking "Voyant" will [load the selected text in Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:11h9j.0/data)

![Text in Voyant](images/single-text-voyant.png)


## Analysis of text collections (corpora) from TextGrid

Not only single texts but complete corpora (text collections) can be transferred directly from TextGrid Repository to Voyant. In the [TextGrid Repository](https://textgridrep.org/repository.html), several texts can be added to a [shelf](/docs/shelf) to create a corpus that is adapted to your own research question. This shelf also offers the option to analyze directly with Voyant Tools.
For example, you can add [12 dramatic texts by Friedrich Schiller](https://textgridrep.org/search?query=&filter=work.genre%3Adrama&filter=edition.agent.value%3ASchiller%2C+Friedrich&start=10&limit=10&order=relevance) to a shelf and then have this collection [analyzed as a whole by Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/aggregator/zip/textgrid:tz6r.0,textgrid:tz9d.0,textgrid:txtj.0,textgrid:tzgk.0,textgrid:tx4z.0,textgrid:tz39.0,textgrid:v0d8.0,textgrid:v0hv.0,textgrid:v0nx.0,textgrid:tz0c.0,textgrid:v0fv.0,textgrid:twt3.0?meta=false&only=text/xml):

![Textsammlung in Voyant](images/corpus-voyant.png)

To find out more about the functionality of the "shelf" option, read more [here](/docs/shelf).


## Added value of integrating Voyant into TextGrid

The integration enables the use of Voyant without any previous knowledge. This means that users do not have to deal with document management. Specifically, this means: You do not need to download the texts from the TextGrid Repository, save them on your own device and then upload them again in Voyant. This service is particularly aimed at learners and teachers from linguistics and literary studies who want to get started with quantitative analysis within the text collections of the TextGrid Repository, such as the [digital library](https://textgrid.de/en/digitale-bibliothek). For example, teachers can send the link to Voyant Tools, created via TextGridRep and containing a specific text collection, directly to the students.


## Other functionalities from Voyant

Voyant in TextGrid shows word frequencies and distributions for selected texts and allows to easily explore and visualize properties of the selected texts. This enables stylistic features such as the use of words in opposite contexts, word clusters at characteristic points in the overall body or in the individual text, to be recognized and displayed at a glance. In the context of literary studies analyzes to support the interpretation from close reading, this can be the impetus to look at one or many texts from a different quantitative perspective.


## Further examples

 - [*Wilhelm Meister's Journeyman Years, or The Renunciations*](https://textgridrep.org/browse/-/browse/11h9j_0#) by [Johann Wolfgang Goethe](https://textgridrep.org/search?filter=edition.agent.value%3aGoethe%2c+Johann+Wolfgang+von) (in German), in  [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:11h9j.0/data)
 - [*The Sandman*](https://textgridrep.org/browse/q5sf_0) by [E. T. A. Hoffmann](https://textgridrep.org/search?filter=edition.agent.value%3aHoffmann%2c+E.+T.+A.) (in German), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:q5sf.0/data)
 - [*Journey around the world in 80 days*](https://textgridrep.org/browse/wr7p_0#tg512.2.1) by [Jules Verne](https://textgridrep.org/search?filter=edition.agent.value%3aVerne%2c+Jules) (in German), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:wr7p.0/data)
 - [*Macbeth*](https://textgridrep.org/browse/vn2s_0) by [William Shakespeare](https://textgridrep.org/search?filter=edition.agent.value%3aShakespeare%2c+William) (in German), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:vn2s.0/data)
 - [*Don Quixote*](https://textgridrep.org/browse/kv63_0) by [Miguel de Cervantes](https://textgridrep.org/search?filter=edition.agent.value%3aCervantes+Saavedra%2c+Miguel+de) (in German), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:kv63.0/data)
 - [*The Divine Comedy*](https://textgridrep.org/browse/m5s9_0) by [Dante Alighieri](https://textgridrep.org/search?filter=edition.agent.value%3aDante+Alighieri) (in German), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:m5s9.0/data)
 - [*La Celestina (Tragicomedia de Calisto y Melibea)*](https://textgridrep.org/browse/3rxbp_0) by [Fernando de Rojas](https://textgridrep.org/search?filter=edition.agent.value%3aFernando+de+Rojas) (in Spanish), in [Voyant](https://voyant-tools.org/?input=https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:3rxb4.0/data)

For the selection of texts it is advisable to take a look at the contents of the TextGridRep, which can be filtered by [author](/facet/edition.agent.value?order=term:asc), by [genre](/facet/work.genre), by [file type](/facet/format) and by [project](https://textgridrep.org/projects).


## Credit

 - [Sinclair, Stéfan and Geoffrey Rockwell, 2016. Voyant Tools. Web. http://voyant-tools.org/](http://voyant-tools.org/)


## Further documentation and tutorials

 - [Voyant Tools documentation](https://voyant-tools.org/docs/#!/guide/start)
 - [Tutorial written by the developer of Voyant](https://voyant-tools.org/docs/#!/guide/tutorial)
 - [Introductory video about Voyant by forTEXT & CATMA](https://www.youtube.com/watch?v=KbBHTlXoZO4) (in German)
 - [Gutierrez de la Torre, Silvia. “Análisis de corpus with Voyant Tools.” The Programming Historian en español, no.3.](https://doi.org/10.46430/phes0043) (In Spanish).


## Integration of the tool in TextGrid

TextGrid uses an instance of [Voyant Tools](https://voyant-tools.org/) that can be accessed directly from the TextGridRep website.


## Further questions?

If you have any further questions about the integration of Voyant and TextGrid, please [contact us](https://textgrid.de/de/kontakt/).
