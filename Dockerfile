FROM gradle:jdk17 as build

COPY . /build
WORKDIR /build
RUN gradle --build-cache assemble

FROM gcr.io/distroless/java17-debian12
#RUN addgroup -S tgrep && adduser -S tgrep -G tgrep
#USER tgrep:tgrep
COPY --from=build /build/build/libs/portal.war portal.war
CMD ["/portal.war"]

